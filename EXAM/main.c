#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"rk4.c"

void HarmOsc(double x, gsl_vector * y, gsl_vector * dydx)
{
	assert(y->size==2);
	// Harmonic osc.. u''=-u -> y1=u, y2=u' -> y1'=y2, y2'=-y1
	gsl_vector_set(dydx,0,gsl_vector_get(y,1));
	gsl_vector_set(dydx,1,-gsl_vector_get(y,0));
}

void function1(double x, gsl_vector * y, gsl_vector * dydx)
{
	// y'=-y/x+3*x
	gsl_vector_set(dydx, 0, 3*x - gsl_vector_get(y,0)*1.0/x);
}



int main()
{
	//
	//
	// I Have chosen exam problem 16, where I implement the runge-kutta 4 method.
	// Here I use the adaptive step controller.
	//
	//
	printf("#EXAM problem 16 - the y_error is the step doubling error i.e. Runge's principle\n\n");
	//	
	// -------------------- Harmonic Oscillator -------------------------

	int n = 2;
	int maxSteps = 2e4; 
	int DRIVER_FAIL=0;

	double a=0; 
	double b=4*M_PI;
	double h = copysign(0.0001,b-a);
	double abs = 1e-6;
	double eps = 1e-6;

	gsl_vector * tPath = gsl_vector_alloc(maxSteps);
	gsl_vector * error = gsl_vector_alloc(maxSteps);
	gsl_matrix * yPath = gsl_matrix_alloc(n,maxSteps); // (y1 y2 ... yn) column vectors

	gsl_vector_set(tPath,0,a);
	gsl_vector_set(error,0,0); 
	gsl_matrix_set(yPath,0,0,1);
	gsl_matrix_set(yPath,0,1,0);
		
	
	int lastStep = driverPathStoring(tPath,error,b,&h,yPath,abs,eps,&rkstep4,&HarmOsc);

	if( lastStep == DRIVER_FAIL ) 
	{
		fprintf(stderr,"Driver couldn't find a solution within %d steps\n",maxSteps);
		return 0;
	}

	printf("# Harmonic Oscillator cosine..\n\n");
	printf("# Number of steps = %i\n\n",lastStep );
	printf("# t \t y_ODE \t y_error \t y_Exact\n");
	for(int i = 0; i < lastStep; i++) 
	{
		double t = gsl_vector_get(tPath,i);
		double y = gsl_matrix_get(yPath,0,i);
		double err = gsl_vector_get(error,i);
		printf("%g %g %g %g\n",t,y,err,cos(t));
	}

	//---------------------- function 1 -------------------------

	// ---- Prep -----
	n=1;
	maxSteps = 1e4; 
	DRIVER_FAIL=0;

	a=1; 
	b=4;
	h = copysign(0.0001,b-a);
	abs = 1e-6;
	eps = 1e-6;
	gsl_vector_set_zero(tPath);
	gsl_vector_set_zero(error);
	gsl_matrix * yPath2 = gsl_matrix_alloc(n,maxSteps);

	// With initial condition y(1)=alpha, for the eq. y'=-y/x+3*x 
	// the solution will be y(x)=x*x+(alpha-1)/x

	gsl_vector_set(tPath,0,a);
	gsl_matrix_set(yPath,0,0,0.3); // setting alpha

	lastStep = driverPathStoring(tPath,error,b,&h,yPath2,abs,eps,&rkstep4,&function1);

	if( lastStep == DRIVER_FAIL ) 
	{
		fprintf(stderr,"Driver couldn't find a solution within %d steps\n",maxSteps);
		return 0;
	}

	printf("\n# Function 1 solution..\n\n");
	printf("# Number of steps = %i\n\n",lastStep );
	printf("# t \t y_ODE \t y_error \t y_Exact\n");
	for(int i = 0; i < lastStep; i++) 
	{
		double t = gsl_vector_get(tPath,i);
		double y = gsl_matrix_get(yPath2,0,i);
		double err = gsl_vector_get(error,i);
		printf("%g %g %g %g\n",t,y,err,t*t+(gsl_matrix_get(yPath2,0,0)-1)/t);
	}

	gsl_vector_free(tPath);
	gsl_vector_free(error);
	gsl_matrix_free(yPath);
	gsl_matrix_free(yPath2);

	return 0;
}