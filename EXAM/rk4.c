void rkstep4(
	double x, double h, gsl_vector * yx,
	void f(double x, gsl_vector * y, gsl_vector * dydx),
	gsl_vector * yx_plus_h, gsl_vector * err
)
{

/*
Runge kutta 4 - here is the Butcher tableau

  0  |
 1/2 | 1/2
 1/2 |  0   1/2
  1  |  0    0    1
~~~~~~~~~~~~~~~~~~~~~~~~~
     | 1/6  1/3  1/3  1/6
*/

	int n = yx -> size;
	gsl_vector * k = gsl_vector_calloc(n);
	gsl_vector * k0 = gsl_vector_alloc(n);
	gsl_vector * k1 = gsl_vector_alloc(n);
	gsl_vector * k2 = gsl_vector_alloc(n);
	gsl_vector * k3 = gsl_vector_alloc(n);
	gsl_vector * temp = gsl_vector_alloc(n);
	// k0
	f(x,yx,k0);
	//k1
	gsl_vector_memcpy(temp,k0);
	gsl_vector_scale(temp,h/2);// temp=h*k0
	gsl_vector_add(temp,yx); // temp = yx + h*k0/2
	f(x+h/2,temp,k1);
	//k2
	gsl_vector_memcpy(temp,k1);
	gsl_vector_scale(temp,h/2);// temp=h*k1/2
	gsl_vector_add(temp,yx); // temp = yx + h*k1/2
	f(x+h/2,temp,k2);
	//k3
	gsl_vector_memcpy(temp,k2);
	gsl_vector_scale(temp,h);// temp=h*k2
	gsl_vector_add(temp,yx); // temp = yx + h*k2
	f(x+h,temp,k3);
	// making final k vector
	gsl_vector_memcpy(temp,k0);
	gsl_vector_scale(temp,1.0/6);
	gsl_vector_add(k,temp);

	gsl_vector_memcpy(temp,k1);
	gsl_vector_scale(temp,1.0/3);
	gsl_vector_add(k,temp);

	gsl_vector_memcpy(temp,k2);
	gsl_vector_scale(temp,1.0/3);
	gsl_vector_add(k,temp);

	gsl_vector_memcpy(temp,k3);
	gsl_vector_scale(temp,1.0/6);
	gsl_vector_add(k,temp);

	for (int i = 0; i < yx->size; i++)
	{
		double y=gsl_vector_get(yx,i);
		gsl_vector_set(yx_plus_h,i,y+h*gsl_vector_get(k,i));
		double yx_plus_h_star=y+h*(gsl_vector_get(k0,i)/6+4*gsl_vector_get(k1,i)/6+gsl_vector_get(k2,i)/6);
		gsl_vector_set(err,i,gsl_vector_get(yx_plus_h,i)-yx_plus_h_star);
	}
	gsl_vector_free(k);
	gsl_vector_free(k0);
	gsl_vector_free(k1);
	gsl_vector_free(k2);
	gsl_vector_free(k3);
	gsl_vector_free(temp);

	//set yx_plus_h and err
}

int driverPathStoring(
	gsl_vector *tPath,
	gsl_vector *errList,
	double b,
	double *h,
	gsl_matrix *yPath,
	double abs,
	double eps,
	void stepper(
		double t, double h, gsl_vector *y,
		void f(double t, gsl_vector *y, gsl_vector *dydt),
		gsl_vector *yh, gsl_vector *err
		),
	void f(double t, gsl_vector *y, gsl_vector *dydt)
) 
{
	int n = yPath->size1;
	int maxSteps = (*tPath).size;
	int step = 0;
	int DRIVER_FAIL=0;
	double a = gsl_vector_get(tPath,0); 
	double tol;
	double normErr; 
	double t;
	double t2;
	double stepDoubling;
	gsl_vector *yh = gsl_vector_alloc(n);
	gsl_vector *err = gsl_vector_alloc(n);
	gsl_vector *yh2 = gsl_vector_alloc(n);
	gsl_vector *y2 = gsl_vector_alloc(n);
	gsl_vector_view y, yNext;

	while( gsl_vector_get(tPath,step) < b ) 
	{
		t = gsl_vector_get(tPath,step);
		y = gsl_matrix_column(yPath,step); 

		if(t+*h>b) 
		{
			*h = b-t;
		}
		t2=t;
		gsl_vector_memcpy(y2,&y.vector); // prep for step doubling
		stepper(t,*h,&y.vector,f,yh,err);

		//step doubling error
		stepper(t2,*h/2,y2,f,yh2,err);
		stepper(t/2+*h/2,*h/2,y2,f,yh2,err);
		stepDoubling = (gsl_vector_get(yh,0)-gsl_vector_get(yh2,0))/((2^4)-1); // 1/(2^p-1), where p is the order of stepper
		//

		tol = ( eps*gsl_blas_dnrm2(yh) + abs ) * sqrt(*h/(b-a));
		normErr = gsl_blas_dnrm2(err);

		if( normErr < tol ) 
		{
			step++;
			if( step+1 > maxSteps ) 
			{	
				return DRIVER_FAIL;
			}
			gsl_vector_set(tPath,step,t+*h);
			yNext = gsl_matrix_column(yPath,step);
			gsl_vector_memcpy(&yNext.vector,yh);
			gsl_vector_set(errList,step,stepDoubling);
		}

		*h *= pow(tol/normErr,0.25)*0.95;
	}

	gsl_vector_free(yh);
	gsl_vector_free(err);
	gsl_vector_free(yh2);
	gsl_vector_free(y2);

	return step+1;
}