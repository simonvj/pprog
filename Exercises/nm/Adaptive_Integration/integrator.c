#include<math.h>
#include<assert.h>
#include"integrator.h"

double f1(double x) 
{
	return sqrt(x);
}

double f2(double x) 
{
	return 1/sqrt(x);
}

double f3(double x) 
{
	return log(x)/sqrt(x);
}

double f4(double x) 
{
	return 1/(x*x);
}

double f5(double x) 
{
	return 1/(1+x*x);
}

double f6(double x)
{
	return x*x*exp(-x*x);
}

double adaptiveOpenRectTrapz(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err,
	double f2,
	double f3,
	int NumbRecursions
) 
{
	assert(NumbRecursions < 1e6);

	double f1 = f(a+(b-a)/6);
	double f4 = f(a+(b-a)*5/6);

	double Q = (2*f1+f2+f3+2*f4)*(b-a)/6;
	double q = (f1+f2+f3+f4)*(b-a)/4;
	*err = fabs(Q-q);
	double tol = abs + eps*fabs(Q);

	if(*err < tol) {
		return Q;
	} else {
		double err1, err2;
		double Q1 = adaptiveOpenRectTrapz(f,a,(a+b)/2,abs/sqrt(2),eps,&err1,f1,f2,NumbRecursions+1);
		double Q2 = adaptiveOpenRectTrapz(f,(a+b)/2,b,abs/sqrt(2),eps,&err2,f3,f4,NumbRecursions+1);
		*err = sqrt( pow(err1,2) + pow(err2,2) );
		return Q1+Q2;
	}
}

double adaptiveIntegratorOpen(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err
) 
{
	double f2 = f(a+(b-a)*2/6);
	double f3 = f(a+(b-a)*4/6);

	int NumbRecursions = 0;
	return adaptiveOpenRectTrapz(f,a,b,abs,eps,err,f2,f3,NumbRecursions);
}

double adaptiveIntegratorOpenWithInf(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err
) 
{

	if( (isinf(a) == -1) & (isinf(b) == 1) ) {
		double pmInf(double t) {
			return f( t/(1-pow(t,2)) ) * (1+pow(t,2))/pow(1-pow(t,2),2);
		}
		double aNew = -1, bNew = 1;
		return adaptiveIntegratorOpen(pmInf,aNew,bNew,abs,eps,err);
	} else if( isinf(a) == -1 ) {
		double plusInf(double t) {
			return f( b-(1-t)/t )/pow(t,2);
		}
		double aNew = 0, bNew = 1;
		return adaptiveIntegratorOpen(plusInf,aNew,bNew,abs,eps,err);
	} else if( isinf(b) == 1 ) {
		double minInf(double t) {
			return f( a+(1-t)/t )/pow(t,2);
		}
		double aNew = 0, bNew = 1;
		return adaptiveIntegratorOpen(minInf,aNew,bNew,abs,eps,err);
	}

	return adaptiveIntegratorOpen(f,a,b,abs,eps,err);
}

double adaptiveClosedRectTrapz(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err,
	double f1,
	double f3,
	int NumbRecursions
) 
{
	assert(NumbRecursions < 1e6);

	double f2 = f((a+b)/2);

	double Q = (f1+2*f2+f3)*(b-a)/4;
	double q = (f1+f2+f3)*(b-a)/2;
	*err = fabs(Q-q);
	double tol = abs + eps*fabs(Q);

	if(*err < tol) {
		return Q;
	} else {
		double err1, err2;
		double Q1 = adaptiveClosedRectTrapz(f,a,(a+b)/2,abs/sqrt(2),eps,&err1,f1,f2,NumbRecursions+1);
		double Q2 = adaptiveClosedRectTrapz(f,(a+b)/2,b,abs/sqrt(2),eps,&err2,f2,f3,NumbRecursions+1);
		*err = sqrt( pow(err1,2) + pow(err2,2) );
		return Q1+Q2;
	}
}

double adaptiveIntegratorClosed(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err
) 
{
	double f1 = f(a);
	double f3 = f(b);

	int NumbRecursions = 0;
	return adaptiveClosedRectTrapz(f,a,b,abs,eps,err,f1,f3,NumbRecursions);
}

double adaptiveIntegratorClosedWithInf(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err
) 
{

	if( (isinf(a) == -1) & (isinf(b) == 1) ) {
		double f(double t) {
			return f( t/(1-pow(t,2)) ) * (1+pow(t,2))/pow(1-pow(t,2),2);
		}
		a = -1; b = 1;
	} else if( isinf(a) == -1 ) {
		double f(double t) {
			return f( b-(1-t)/t )/pow(t,2);
		}
		a = 0; b = 1;
	} else if( isinf(b) == 1 ) {
		double f(double t) {
			return f( a+(1-t)/t )/pow(t,2);
		}
		a = 0; b = 1;
	}

	return adaptiveIntegratorClosed(f,a,b,abs,eps,err);

}