#ifndef HAVE_INTEGRATOR_H
#define HAVE_INTEGRATOR_H

double f1(double x);
double f2(double x);
double f3(double x);
double f4(double x);
double f5(double x);
double f6(double x);
double adaptiveClosedRectTrapz(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err,
	double f1,
	double f3,
	int noRecursions
);
double adaptiveOpenRectTrapz(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err,
	double f2,
	double f3,
	int noRecursions
);
double adaptiveIntegratorClosed(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err
);
double adaptiveIntegratorOpen(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err
);

double adaptiveIntegratorOpenWithInf(
	double f(double),
	double a,
	double b,
	double abs,
	double eps,
	double *err
);

#endif