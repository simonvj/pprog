#include <stdio.h>
#include <math.h>
#include <gsl/gsl_matrix.h>
#include "integrator.h"
#include "../ODE/ode.h"

int main()
{
	//------------ Part A -----------
	printf("#------Part A------\n");
	double a = 0, b = 1, abs = 1e-6, eps = 1e-6, err;

	double Q1 = adaptiveIntegratorOpen(f1,a,b,abs,eps,&err);
	printf("#Numerical: Int_0^1 sqrt(x) dx = %g with error = %g\n",Q1,err);
	printf("#Precise value: 2/3\n\n");

	double Q2 = adaptiveIntegratorOpen(f2,a,b,abs,eps,&err);
	printf("#Numerical: Int_0^1 1/sqrt(x) dx = %g with error = %g\n",Q2,err);
	printf("#Precise value: 2\n\n");

	double Q3 = adaptiveIntegratorOpen(f3,a,b,abs,eps,&err);
	printf("#Numerical: Int_0^1 ln(x)/sqrt(x) dx = %g with error = %g\n",Q3,err);
	printf("#Precise value: -4\n\n");

	abs = 1e-8; eps = 1e-8;

	// making function here to check number of evaluations
	int NumbEval = 0;
	double fPi(double x)
	{
		NumbEval++;
		return 4*sqrt(1-pow(1-x,2));
	};
	
	double QPi = adaptiveIntegratorOpen(fPi,a,b,abs,eps,&err);
	printf("#Numerical: Int_0^1 4*sqrt(1-(1-x)^2) dx = %.15g with error = %lg\n",QPi,err);
	printf("#Precise value: %.15g(pi)\n",M_PI);
	printf("#Did a total of %d evaluations\n\n",NumbEval);

	//------------ Part B ------------------
	printf("#------Part B------\n");
	a=1; b=INFINITY; abs = 1e-6; eps = 1e-6;

	double QInf1 = adaptiveIntegratorOpenWithInf(f4,a,b,abs,eps,&err);
	printf("#Numerical: Int_1^Inf 1/x² dx = %g with error = %g\n",QInf1,err);
	printf("#Precise value: 1\n\n");

	a=0;	

	double QInf2 = adaptiveIntegratorOpenWithInf(f5,a,b,abs,eps,&err);
	printf("#Numerical: Int_1^Inf 1/x² dx = %.15g with error = %g\n",QInf2,err);
	printf("#Precise value: %.15g(pi/2)\n\n",M_PI/2);

	//------------ Part C ------------------
	printf("#------Part C------\n");
	a = 0, b = 2, abs = 1e-6, eps = 1e-6;
	gsl_vector * y = gsl_vector_alloc(1);
	gsl_vector_set(y,0,a);
	double h = copysign(0.1,b-a);

	double Q = adaptiveIntegratorOpen(f6,a,b,abs,eps,&err);
	double I = ODEIntegratorRK4(a,b,h,y,abs,eps,&funcGaussWithSquare);

	printf("#Numerical Adaptive Integration: Int x²*exp(-x²) dx form 0 to 2 = %.17g with error = %g\n", Q,err);
	printf("#Numerical ODE: Int_x²*exp(-x²) dx form 0 to 2 = %.17g\n",I);
	printf("#Difference: AdaptiveIntegration-ODE = %.17g\n", Q-I );
	printf("#Precise value: 1/4*sqrt(pi)*erf(2)-1/(e⁴) = %.15g\n", 1./4*sqrt(M_PI)*erf(2)-1/pow(exp(1),4));
	printf("#ODE method is most precise in this case!\n");



	return 0;
}