#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>
#include<stdio.h>
#include<math.h>
#include"ann.h"

ann* ann_alloc(int hidden_neurons, double(*f)(double))
{
	ann* network = malloc(sizeof(ann));
	network->n=hidden_neurons;
	network->f=f;
	network->data=gsl_vector_alloc(3*hidden_neurons);
	return network;
}

void ann_free(ann* network)
{
	gsl_vector_free(network->data);
	free(network);
}

double ann_feed_forward(ann* network, double x)
{
	double s=0;
	for(int i=0; i<network->n; i++)
	{
		double a=gsl_vector_get(network->data,0*network->n + i);
		double b=gsl_vector_get(network->data,1*network->n + i);
		double w=gsl_vector_get(network->data,2*network->n + i);
		s+=network->f((x+a)/b)*w;
	}
	return s;
}

void ann_train(ann* network, gsl_vector * x_input, gsl_vector * y_output)
{
	double delta(gsl_vector * p)
	{
		gsl_vector_memcpy(network->data,p);
		double s=0;
		for(int i=0; i<x_input->size; i++)
		{
			double x=gsl_vector_get(x_input,i);
			double f=gsl_vector_get(y_output,i);
			double y=ann_feed_forward(network,x);
			s+=fabs(y-f);
		}
		return s/x_input->size;
	}

	gsl_vector* p = gsl_vector_alloc(network->data->size);
	gsl_vector* step_size = gsl_vector_alloc(network->data->size);
	gsl_vector_memcpy(p, network->data); 
	gsl_vector_set_all(step_size, 0.1);

	gsl_multimin_function F;
	F.f = delta;
	F.n = p->size;
	F.params = NULL;

	gsl_multimin_fminimizer *s = 
	gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2, F.n);		
	gsl_multimin_fminimizer_set (s, &F, p, step_size);
	
	int iter = 0, status;
	do
	{
		iter++;
		int iter_status = gsl_multimin_fminimizer_iterate(s);
		if(iter_status != 0)
		{
			fprintf(stderr, "Cannot improve\n");
			break;
		}
		double acc = 1e-3;
		status = gsl_multimin_test_size(s->size, acc);
		if(status == GSL_SUCCESS) 
		{	
			fprintf(stderr, "ann converged in %i iterations\n", iter);
		}

	}
	while
	( 
		status == GSL_CONTINUE && iter < 1e+6
	);

	gsl_vector_memcpy(network->data, s->x); 
	
	gsl_vector_free(p);
	gsl_vector_free(step_size);	
	gsl_multimin_fminimizer_free(s);
}