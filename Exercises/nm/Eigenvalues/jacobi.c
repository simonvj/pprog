#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#define RND rand()%21+(-10) // random natural number from -10 to 10

void rand_sym_matrix(gsl_matrix * A)
{
	int n = A->size1; //dimension - sym matrix is a square
	for(int i=0; i<n; i++)
	{
		for(int k=i;k<n;k++)
		{
			if(i==k) 
			gsl_matrix_set(A,i,k,RND);
			else
			{
				int f=RND;
				gsl_matrix_set(A,i,k,f);
				gsl_matrix_set(A,k,i,f);
			}
		}
	}
}

//----------------- Part A ----------------------

int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V)
{
/* Jacobi diagonalization; upper triangle of A is destroyed;
   e and V accumulate eigenvalues and eigenvectors */
	int changed, NumOfRot=0, n=A->size1;

	for(int i=0;i<n;i++)
	{
		gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
	}
	gsl_matrix_set_identity(V);

	do
	{ 
		changed=0; 

		for(int p=0;p<n;p++)for(int q=p+1;q<n;q++)
		{
			double app=gsl_vector_get(e,p);
			double aqq=gsl_vector_get(e,q);
			double apq=gsl_matrix_get(A,p,q);
			double phi=0.5*atan2(2*apq,aqq-app);
			double c = cos(phi), s = sin(phi);
			double app1=c*c*app-2*s*c*apq+s*s*aqq;
			double aqq1=s*s*app+2*s*c*apq+c*c*aqq;
			if(app1!=app || aqq1!=aqq)
			{ 
					changed=1;
					NumOfRot++;
					gsl_vector_set(e,p,app1);
					gsl_vector_set(e,q,aqq1);
					gsl_matrix_set(A,p,q,0.0);
					for(int i=0;i<p;i++)
					{
						double aip=gsl_matrix_get(A,i,p);
						double aiq=gsl_matrix_get(A,i,q);
						gsl_matrix_set(A,i,p,c*aip-s*aiq);
						gsl_matrix_set(A,i,q,c*aiq+s*aip); 
					}
					for(int i=p+1;i<q;i++)
					{
						double api=gsl_matrix_get(A,p,i);
						double aiq=gsl_matrix_get(A,i,q);
						gsl_matrix_set(A,p,i,c*api-s*aiq);
						gsl_matrix_set(A,i,q,c*aiq+s*api); 
					}
					for(int i=q+1;i<n;i++){
						double api=gsl_matrix_get(A,p,i);
						double aqi=gsl_matrix_get(A,q,i);
						gsl_matrix_set(A,p,i,c*api-s*aqi);
						gsl_matrix_set(A,q,i,c*aqi+s*api); 
					}
					for(int i=0;i<n;i++){
						double vip=gsl_matrix_get(V,i,p);
						double viq=gsl_matrix_get(V,i,q);
						gsl_matrix_set(V,i,p,c*vip-s*viq);
						gsl_matrix_set(V,i,q,c*viq+s*vip); 
					}
			} 
		} 
	}while(changed!=0);

return NumOfRot; 
}

//----------------- Part B ----------------------

int jacobi_eig_by_eig(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int p, double phase)
{
	int changed, NumOfRot=0, n=A->size1;
	assert(p>=0 && p<n-1);

	do
	{ 
		changed=0; 

		for(int q=p+1;q<n;q++)
			{
				double app=gsl_vector_get(e,p);
				double aqq=gsl_vector_get(e,q);
				double apq=gsl_matrix_get(A,p,q);
				double phi=phase+atan2(2*apq,aqq-app)/2;
				double c = cos(phi), s = sin(phi);
				double app1=c*c*app-2*s*c*apq+s*s*aqq;
				double aqq1=s*s*app+2*s*c*apq+c*c*aqq;

				if(app1!=app || aqq1!=aqq)
					{ 
						NumOfRot++;
						changed=1;
						gsl_vector_set(e,p,app1);
						gsl_vector_set(e,q,aqq1);
						gsl_matrix_set(A,p,q,0.0);

						for(int i=p+1;i<q;i++)
						{
							double api=gsl_matrix_get(A,p,i);
							double aiq=gsl_matrix_get(A,i,q);
							gsl_matrix_set(A,p,i,c*api-s*aiq);
							gsl_matrix_set(A,i,q,c*aiq+s*api); 
						}
						for(int i=q+1;i<n;i++){
							double api=gsl_matrix_get(A,p,i);
							double aqi=gsl_matrix_get(A,q,i);
							gsl_matrix_set(A,p,i,c*api-s*aqi);
							gsl_matrix_set(A,q,i,c*aqi+s*api); 
						}
						for(int i=0;i<n;i++){
							double vip=gsl_matrix_get(V,i,p);
							double viq=gsl_matrix_get(V,i,q);
							gsl_matrix_set(V,i,p,c*vip-s*viq);
							gsl_matrix_set(V,i,q,c*viq+s*vip); 
						}
			} 
		} 
	}while(changed!=0);
	return NumOfRot;
}

int jacobiNumberEigenvalue(gsl_matrix *A, gsl_vector * e, gsl_matrix * V, int NumEigVal, double phase) 
{
	int n = A->size1;
	assert( n == A->size2 );
	assert( NumEigVal > 0 && NumEigVal <= n );

	for(int i = 0; i < n; i++) {
		gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
		for(int j = 0; j < n; j++) {
			gsl_matrix_set(V,i,j,(i == j) ? 1 : 0); // if (i==j) is true return 1 otherwise 0
		}
	}

	int noOfRotations = 0;
	for(int i = 0; i < NumEigVal && i < n-1; i++) {
		noOfRotations += jacobi_eig_by_eig(A,e,V,i,phase);
	}
	return noOfRotations;
}

int jacobiNumberLowestEigenvalue(gsl_matrix * A, gsl_vector * e, gsl_matrix * V, int NumEigVal) 
{
	double phase = 0;
	return jacobiNumberEigenvalue(A,e,V,NumEigVal,phase);
}

int jacobiNumberHighestEigenvalue(gsl_matrix * A, gsl_vector * e, gsl_matrix * V, int NumEigVal) 
{
	double phase = M_PI/2;
	return jacobiNumberEigenvalue(A,e,V,NumEigVal,phase);
}

//----------------- Part C ----------------------

void largestElementInEachUpperTriangularRow(const gsl_matrix *A, gsl_vector *L) 
{
	int n = (*L).size, m = (*A).size2;
	assert( n == (*A).size1-1 );

	double max = -INFINITY, curr;
	for(int i = 0; i < n; i++) 
	{
		for(int j = i+1; j < m; j++) 
		{
			curr = gsl_matrix_get(A,i,j);
			if(curr > max) 
			{
				max = curr;
				gsl_vector_set(L,i,j);// index number
			}
		}
	}
}


void findLargestElementIndex(int *p, int *q, const gsl_matrix *A, const gsl_vector *L) 
{
	int n = (*L).size;

	double max = -INFINITY, curr;
	for(int i = 0; i < n; i++) 
	{
		curr = gsl_matrix_get(A,i,gsl_vector_get(L,i));
		if( curr > max ) 
		{
			max = curr;
			*p = i;
			*q = gsl_vector_get(L,i);
		}
	}
}

void updateLargestElementInRow(int i, int j, const gsl_matrix *A, gsl_vector *L) 
{
	double currMax = gsl_matrix_get(A,i,gsl_vector_get(L,i));
	double newElement = gsl_matrix_get(A,i,j);
	if( newElement > currMax ) 
	{
		gsl_vector_set(L,i,j);
	}
}

int jacobi_Classic_Eigenvalue(gsl_matrix *A, gsl_vector * e, gsl_matrix * V) 
{
	int n = (*A).size1;
	assert( n == (*A).size2 );

	gsl_vector *L = gsl_vector_alloc(n-1);
	largestElementInEachUpperTriangularRow(A,L);

	for(int i = 0; i < n; i++) 
	{
		gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
		for(int j = 0; j < n; j++) 
		{
			gsl_matrix_set(V,i,j,(i == j) ? 1 : 0);
		}
	}

	double c, s, theta, App, Aqq, Apq;

	int iterate = 1, noOfRotations = 0;
	while( iterate ) {
		iterate = 0;

		int p, q;
		findLargestElementIndex(&p,&q,A,L);

		App = gsl_vector_get(e,p);
		Aqq = gsl_vector_get(e,q);
		Apq = gsl_matrix_get(A,p,q);
		theta = atan2(2*Apq, Aqq - App) / 2;
		c = cos(theta);
		s = sin(theta);
		double AppCalc = c*c*App-2*s*c*Apq+s*s*Aqq;
		double AqqCalc = s*s*App+2*s*c*Apq+c*c*Aqq;

		if( App != AppCalc || Aqq != AqqCalc ) 
		{
			noOfRotations++;
			iterate = 1;
			gsl_vector_set(e,p,AppCalc);
			gsl_vector_set(e,q,AqqCalc);
			gsl_matrix_set(A,p,q,0.0);

			for(int i = 0; i < p; i++) 
			{
				double Aip = gsl_matrix_get(A,i,p);
				double Aiq = gsl_matrix_get(A,i,q);
				gsl_matrix_set(A,i,p,c*Aip-s*Aiq);
				gsl_matrix_set(A,i,q,s*Aip+c*Aiq);
				updateLargestElementInRow(i,p,A,L);
				updateLargestElementInRow(i,q,A,L);
			}

			for(int i = p + 1; i < q; i++) 
			{
				double Api = gsl_matrix_get(A,p,i);
				double Aiq = gsl_matrix_get(A,i,q);
				gsl_matrix_set(A,p,i,c*Api-s*Aiq);
				gsl_matrix_set(A,i,q,s*Api+c*Aiq);
				updateLargestElementInRow(p,i,A,L);
				updateLargestElementInRow(i,q,A,L);
			}

			for(int i = q + 1; i < n; i++) 
			{
				double Api = gsl_matrix_get(A,p,i);
				double Aqi = gsl_matrix_get(A,q,i);
				gsl_matrix_set(A,p,i,c*Api-s*Aqi);
				gsl_matrix_set(A,q,i,s*Api+c*Aqi);
				updateLargestElementInRow(p,i,A,L);
				updateLargestElementInRow(q,i,A,L);
			}

			for(int i = 0; i < n; i++) 
			{
				double Vip = gsl_matrix_get(V,i,p);
				double Viq = gsl_matrix_get(V,i,q);
				gsl_matrix_set(V,i,p,c*Vip-s*Viq);
				gsl_matrix_set(V,i,q,s*Vip+c*Viq);
			}
		}
	}

	gsl_vector_free(L);

	return noOfRotations;
}