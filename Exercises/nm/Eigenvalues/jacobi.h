#ifndef HAVE_JACOBI_H
#define HAVE_JACOBI_H

void rand_sym_matrix(gsl_matrix * A);
int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
int jacobi_eig_by_eig(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int p, double phase);
int jacobiNumberEigenvalue(gsl_matrix *A, gsl_vector * e, gsl_matrix * V, int noEigVal, double phase);
int jacobiNumberLowestEigenvalue(gsl_matrix * A, gsl_vector * e, gsl_matrix * V, int NumEigVal);
int jacobiNumberHighestEigenvalue(gsl_matrix * A, gsl_vector * e, gsl_matrix * V, int NumEigVal);
int jacobi_Classic_Eigenvalue(gsl_matrix *A, gsl_vector * e, gsl_matrix * V);

#endif