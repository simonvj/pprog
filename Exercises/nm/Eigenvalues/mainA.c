#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"jacobi.h"
#include"../Linear_Equations/matrix_opt.h"

int main(int argc, char** argv)
{
	int n=3;
	if(argc>1)
	{
			n=atoi(argv[1]);	
	}

	// Generate random symmetric matrix
	gsl_matrix * A = gsl_matrix_calloc(n,n);
	gsl_matrix * AA = gsl_matrix_alloc(n,n);
	gsl_matrix * temp = gsl_matrix_alloc(n,n);
	gsl_matrix * VTAV = gsl_matrix_calloc(n,n);
	gsl_matrix *V = gsl_matrix_alloc(n,n);
	gsl_vector *e = gsl_vector_alloc(n); // Diagonal elements of matrix D

	rand_sym_matrix(A);
	gsl_matrix_memcpy(AA,A);

	int sweeps=jacobi(A,e,V); 
	if(n<9)
	{
		printf("-------- Part 1 --------\n\n");
		printf("n=%i, sweeps=%i\n",n,sweeps);
		matrix_print("A=",AA);
		matrix_print("V=", V);
		vector_print("eigenvalues=", e);
		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,AA,0,temp);
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,temp,V,0,VTAV);
		matrix_print("VTAV=", VTAV);
	} 
	else
	{
		printf("%i\t", n); // follow iterations
	}

	gsl_matrix_free(A);
	gsl_matrix_free(AA);
	gsl_matrix_free(V);
	gsl_matrix_free(temp);
	gsl_matrix_free(VTAV);
	gsl_vector_free(e);

	return 0;
}