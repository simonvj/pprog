#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"jacobi.h"

int main(int argc, char *argv[]) 
{
	int n, NumEigVal;

	if(argc < 3) 
	{
		n = 12;
		NumEigVal = n/2;
		printf("No argument passed.. Matrix size is (%dx%d) and %d eigenvalues will be found.\n",n,n,NumEigVal);
	} 
	else 
	{
		n = atoi(argv[1]);
		NumEigVal = atoi(argv[2]);
		if(argc==3)
		{
		printf("Size of matrix is (%d,%d) and number of eigenvalues to be found is %d\n",n,n,NumEigVal);
		printf("The largest eigenvalues can be found by adding a phase of pi/2 to the rotation angle\n\n");
		}
	}
	gsl_matrix * A = gsl_matrix_alloc(n,n);
	gsl_matrix * AA = gsl_matrix_alloc(n,n);
	gsl_matrix * V = gsl_matrix_alloc(n,n);
	gsl_matrix * VV = gsl_matrix_alloc(n,n);
	gsl_vector * e = gsl_vector_alloc(n);
	gsl_vector * ee = gsl_vector_alloc(n);

	rand_sym_matrix(A);
	gsl_matrix_memcpy(AA,A);

	int noOfRotations1 = jacobiNumberLowestEigenvalue(A,e,V,NumEigVal);

	if(argc <= 3)
	{
		int noOfRotations2 = jacobiNumberHighestEigenvalue(AA,ee,VV,NumEigVal);
		printf("Number of rotations: %d\n\n",noOfRotations1);
		printf("Eigenvalue vector - %d smallest elements.\n",NumEigVal);
		printf("e=\n");
	for (int i = 0; i < NumEigVal; ++i)
	{
		printf("\t%g\n", gsl_vector_get(e,i));
	}


	printf("Number of rotations: %d\n\n",noOfRotations2);

	printf("Eigenvalue vector - %d largest elements.\n",NumEigVal);
	printf("e=\n");
	for (int i = 0; i < NumEigVal; ++i)
	{
		printf("\t%g\n", gsl_vector_get(ee,i));
	}
	}
	else
	{
		printf("%i\t", n); // follow iterations
	}

	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(e);
	gsl_matrix_free(AA);
	gsl_matrix_free(VV);
	gsl_vector_free(ee);

	return 0;
}