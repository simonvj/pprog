#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<assert.h>
#include"jacobi.h"

/*
	First argument = size of the symmetric square matrix
	Second argument = 1 NumOfRot for cyclic mehtod and for classic method.or 2 classic and n
*/

int main(int argc, char *argv[]) 
{
	assert(argc > 2);
	assert( atoi(argv[2]) != 1 || atoi(argv[2]) != 2 );

	int n = atoi(argv[1]);

	gsl_matrix *A = gsl_matrix_alloc(n,n);
	gsl_matrix * V = gsl_matrix_alloc(n,n);
	gsl_vector *e = gsl_vector_alloc(n);

	rand_sym_matrix(A);

	int NumOfRot1, NumOfRot2;
	//Full diagonalization of the matrix of size (%d,%d).
	if( atoi(argv[2]) == 1 ) 
	{
		NumOfRot1 = jacobi(A,e,V);
		NumOfRot2 = jacobi_Classic_Eigenvalue(A,e,V);
		printf("%i %i %i\n",n, NumOfRot1, NumOfRot2);
	} 
	else if( atoi(argv[2]) == 2 ) 
	{
		jacobi_Classic_Eigenvalue(A,e,V);
	}


	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(e);

	return 0;
}