#include<assert.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"../Linear_Equations/matrix_opt.h"
#include"../Eigenvalues/jacobi.h"
#include"LSF.h"

void LS_fit(int m, double f(int i,double x),gsl_vector* x, gsl_vector* y, gsl_vector* dy, gsl_vector* c, gsl_matrix* S)
{
	int n = x->size;

	gsl_matrix *A    = gsl_matrix_alloc(n,m);
	gsl_vector *b    = gsl_vector_alloc(n);
	gsl_matrix *R    = gsl_matrix_alloc(m,m);
	gsl_matrix* invR = gsl_matrix_alloc(m,m);
	gsl_matrix *I    = gsl_matrix_alloc(m,m);

	for(int i=0;i<n;i++)
	{
		double xi  = gsl_vector_get(x ,i);
		double yi  = gsl_vector_get(y ,i);
		double dyi = gsl_vector_get(dy,i); assert(dyi>0);
		gsl_vector_set(b,i,yi/dyi);

		for(int k=0;k<m;k++)
		{
			gsl_matrix_set(A,i,k,f(k,xi)/dyi);
		}
	}

	QR_GS_decompose(A,R);
	QR_GS_solve(A,R,b,c);

	gsl_matrix_set_identity(I);
	QR_GS_inverse(I,R,invR);
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,invR,invR,0,S);
	
	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_matrix_free(R);
	gsl_matrix_free(invR);
	gsl_matrix_free(I);
}

void Singular_Value_Decomposition(gsl_matrix *A, gsl_matrix * S, gsl_matrix * V)
{
int n=A->size1, m=A->size2;
assert(n>m && S->size1==m && S->size2==m && V->size1==m && V->size2==m);
gsl_vector * e = gsl_vector_alloc(m);
gsl_matrix * A_copy =gsl_matrix_alloc(n,m);
gsl_matrix * ATA =gsl_matrix_alloc(m,m);
gsl_matrix * Q =gsl_matrix_alloc(m,m);
gsl_matrix * D = gsl_matrix_alloc(m,m);

gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,A,A,0,Q); // temp ATA
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,A,A,0,ATA);
jacobi(Q,e,V); // get eigenvalues and vectors
gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,V,ATA,0,Q); //VATA
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,Q,V,0,D); // D=VATAV

gsl_matrix_set_identity(S);

for(int i=0;i<S->size1;i++)
{
	gsl_matrix_set(S,i,i,sqrt(gsl_matrix_get(D,i,i)));
}

for(int i=0;i<D->size1;i++)
{
	double dm = gsl_matrix_get(D,i,i);
	gsl_matrix_set(D,i,i,1/sqrt(dm));
}

gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,A,D,0,A_copy);

for(int i=0;i<A->size1;i++)
{
	for(int j=0;j<A->size2;j++)
	{
		gsl_matrix_set(A,i,j,gsl_matrix_get(A_copy,i,j));
	}
}

gsl_matrix_free(A_copy);
gsl_matrix_free(ATA);
gsl_matrix_free(Q);
gsl_matrix_free(D);
gsl_vector_free(e);
}

gsl_matrix * LS_fit_SVD(gsl_vector * x, gsl_vector * y, gsl_vector * dy, int numfunc, double func(int m, double x), gsl_vector * c)
{
int n = x->size;
gsl_matrix* A = gsl_matrix_alloc(n,numfunc);
gsl_matrix* ATA = gsl_matrix_alloc(numfunc,numfunc);
gsl_matrix* U = gsl_matrix_alloc(n,numfunc);
gsl_matrix* S = gsl_matrix_alloc(numfunc,numfunc);
gsl_matrix* Sigma = gsl_matrix_alloc(numfunc,numfunc);
gsl_matrix* invS = gsl_matrix_alloc(numfunc,numfunc);
gsl_matrix* V = gsl_matrix_alloc(numfunc,numfunc);
gsl_matrix* temp = gsl_matrix_alloc(numfunc,numfunc);
gsl_vector* b = gsl_vector_alloc(n);
gsl_vector* cc = gsl_vector_alloc(numfunc);

for(int i=0; i<n;i++)
{
	gsl_vector_set(b,i,gsl_vector_get(y,i)/gsl_vector_get(dy,i));
	double xi = gsl_vector_get(x,i);
	double dyi = gsl_vector_get(dy,i);
	for(int j=0;j<numfunc;j++)
	{
		gsl_matrix_set(A,i,j,func(j,xi)/dyi);
	}
}

gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,A,A,0,ATA);

jacobi(ATA,x,V);

for(int i=0; i<numfunc;i++)
{
	gsl_matrix_set(S,i,i,sqrt(gsl_vector_get(x,i)));
	gsl_matrix_set(invS,i,i,1/sqrt(gsl_vector_get(x,i)));
	gsl_matrix_set(Sigma,i,i,1/(gsl_vector_get(x,i)));
}

gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,V,invS,0,temp);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,A,temp,0,U);

QR_GS_solve(U,S,b,c);
gsl_blas_dgemv(CblasNoTrans,1,V,c,0,cc);

for(int i=0;i<c->size;i++)
{
	gsl_vector_set(c,i,gsl_vector_get(cc,i));
}

gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,V,Sigma,0,temp);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,temp,V,0,Sigma);

gsl_matrix_free(A);
gsl_matrix_free(ATA);
gsl_matrix_free(invS);
gsl_matrix_free(V);
gsl_vector_free(b);
gsl_vector_free(cc);
return Sigma;
}

