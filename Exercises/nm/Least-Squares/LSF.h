#ifndef HAVE_LSF_H
#define HAVE_LSF_H
#include<gsl/gsl_matrix.h>

void LS_fit(int m, double f(int i,double x),	gsl_vector* x, gsl_vector* y, gsl_vector* dy, gsl_vector* c, gsl_matrix* S);
void Singular_Value_Decomposition(gsl_matrix *A, gsl_matrix * S, gsl_matrix * V);
gsl_matrix * LS_fit_SVD(gsl_vector * x, gsl_vector * y, gsl_vector * dy, int numfunc, double func(int m, double x), gsl_vector * c);

#endif

