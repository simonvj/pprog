#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"LSF.h"
#include"../Linear_Equations/matrix_opt.h"

int main()
{
	// Data to fit 
	double x[] = {0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9};
	double y[] = {-15.3, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75};
	double dy[] = {1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478};

	int n=sizeof(x)/sizeof(x[0]); // number of elements, sizeof returns amount of data allocated
	int m=3; // number of coefficients

	// Allocation of vectors and matrices
	gsl_vector * vx = gsl_vector_alloc(n);
	gsl_vector * vy = gsl_vector_alloc(n);
	gsl_vector * vdy = gsl_vector_alloc(n);
	gsl_vector * c = gsl_vector_alloc(m);
	gsl_matrix * S = gsl_matrix_alloc(m,m);

	double funs(int i, double x)
	{
    	switch(i)
    	{
    		case 0: return log(x); break;
    		case 1: return 1.0;   break;
    		case 2: return x;     break;
    		default: {fprintf(stderr,"funs: wrong i:%d",i); return 0;}
    	}
	}

	// Put into vectors and print
	for(int i=0;i<n;i++)
		{
		printf("%g %g %g\n",x[i],y[i],dy[i]); //print data
		gsl_vector_set(vx,i,x[i]);
		gsl_vector_set(vy,i,y[i]);
		gsl_vector_set(vdy,i,dy[i]);
		}printf("\n\n");

	S = LS_fit_SVD(vx,vy,vdy,m,funs,c);

	matrix_print("S=",S);
	gsl_vector * dc = gsl_vector_alloc(m);

	for(int k=0;k<m;k++)
	{
		gsl_vector_set(dc,k,sqrt(fabs(gsl_matrix_get(S,k,k))));
	}
	vector_print("dc=",dc);

	double fit(double x)
	{
		double s=0;
		for(int k=0;k<m;k++)
		{
			s+=gsl_vector_get(c,k)*funs(k,x);
		}
		return s;
	}

	double fit_plus(int i, double x)
	{
		return fit(x)+gsl_vector_get(dc,i)*funs(i,x);
	}

	double fit_minus(int i, double x)
	{
		return fit(x)-gsl_vector_get(dc,i)*funs(i,x);
	}

	double z,dz=(x[n-1]-x[0])/90;

	for(int i=0;i<m;i++)
	{	
		printf("#z\tfit(z)\tfit(z)_plus\tfit(z)_minus\tfunc=%i \n",i);
		z=x[0]-dz/2;
		do
		{
			printf("%g %g %g %g\n",z,fit(z),fit_plus(i,z),fit_minus(i,z));
			z+=dz;
		}while(z<x[n-1]+dz);
	printf("\n\n");
	}

			
//FREE
gsl_vector_free(vx);
gsl_vector_free(dc);
gsl_vector_free(vy);
gsl_vector_free(vdy);
gsl_vector_free(c);
gsl_matrix_free(S);

return 0;
}