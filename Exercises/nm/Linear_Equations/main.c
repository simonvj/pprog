#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"matrix_opt.h"

int main()
{
	printf("------------PART A------------\n\n");
	printf("We have a matrix A and will use QR factorization via Gram-Schimidt mehtod to decompose it into A=QR. Where R is upper triangular\n\n");

	//Prep matrix A

	size_t n =5, m=4; //Remember n>=m !!

	gsl_matrix * A = gsl_matrix_alloc(n,m);
	matrix_rand(A);
	matrix_print("A=", A);

	//Prep Q and R

	gsl_matrix * R = gsl_matrix_alloc(m,m);
	gsl_matrix * Q = gsl_matrix_alloc(n,m);
	gsl_matrix_memcpy(Q, A); //gsl copy of matrix

	//Decompose 

	QR_GS_decompose(Q,R); 
	matrix_print("R=", R);
	matrix_print("Q=", Q);

	// Checking 

	gsl_matrix * QTQ = gsl_matrix_calloc(m,m);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans, 1.0, Q,Q,0.0, QTQ );
	printf("Checking (Q^T)Q=1..\n\n");
	matrix_print("QTQ=", QTQ);
	printf("Works. However not entirely zero, because of machine uncertainties..\n\n");

	gsl_matrix * QR = gsl_matrix_calloc(n,m);
    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, Q, R, 0.0, QR);
    printf("Checking QR=A..\n\n");
    matrix_print("QR=",QR);
    printf("Works.\n\n");

    //solving QRx=b

    printf("Now we solve the system Ax=QRx=b, by using QTQ=1 and doing backsubstitution..\n\n");

    //prep system QRx=b - square matrix!!

    n =4; // square matrix

	A = gsl_matrix_alloc(n,n);
	R = gsl_matrix_alloc(n,n);
	Q = gsl_matrix_alloc(n,n);
	gsl_vector * b = gsl_vector_calloc(m);
    gsl_vector * x = gsl_vector_calloc(m);
	matrix_rand(A);
	vector_rand(b);
	gsl_matrix_memcpy(Q, A);
	QR_GS_decompose(Q,R); 

	matrix_print("A=", A);
    vector_print("b=", b);

    QR_GS_solve(Q,R,b,x);

    vector_print("x=", x);

    // Checking Ax=b

    gsl_vector * b_check = gsl_vector_calloc(n);
	gsl_blas_dgemv(CblasNoTrans, 1.0, A,x,0.0, b_check);
    printf("Checking Ax=b..\n\n");
    vector_print("Ax=",b_check);
    printf("Works\n\n");

    printf("------------PART B------------\n\n");

    gsl_matrix * invA = gsl_matrix_calloc(n,n);
    gsl_matrix * AinvA = gsl_matrix_calloc(n,n);
    QR_GS_inverse(Q,R,invA); // here we use the Q and R from the decomposition
    matrix_print("invA=",invA);
    //checking
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A,invA,0,AinvA);
    matrix_print("AinvA=",AinvA);
    printf("Works.\n");

    printf("------------PART C------------\n\n");
    //Ux=v
    gsl_matrix_set_zero(A);
    gsl_matrix_set_zero(Q);
    gsl_matrix * Acpy = gsl_matrix_calloc(n,n);
    matrix_rand(A);
    gsl_matrix_memcpy(Acpy,A);
    vector_rand(b);
    gsl_vector_memcpy(x,b);
    matrix_print("A=",A);
    vector_print("b=",b);

    QR_Givens(A); //decompose
    QR_Givens_solve(A,x); // x is composed

    //checking
    vector_print("x=",x); // solution
    gsl_blas_dgemv(CblasNoTrans,1.0,Acpy,x,0.0,b);
    vector_print("Ax=b=",b);

    QR_Givens_unpack_Q(A,Q);
    matrix_print("Q=",Q);
    gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,Q,Q,0.0,QTQ);
    matrix_print("QTQ = identity?",QTQ); //check QTQ=1

    gsl_matrix_set_zero(invA);
    QR_Givens_inverse(A,invA);
    gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,Acpy,invA,0,AinvA);
    matrix_print("AinvA = identity?",AinvA);

    printf("---------- Conclusion = All works! ----------\n");


    // Free memory
	gsl_matrix_free(A);
	gsl_matrix_free(Acpy);
	gsl_matrix_free(Q);
	gsl_matrix_free(R);
	gsl_matrix_free(invA);
	gsl_matrix_free(AinvA);
	gsl_matrix_free(QTQ);
	gsl_matrix_free(QR);
	gsl_vector_free(b);
	gsl_vector_free(b_check);
	gsl_vector_free(x);
	return 0;
}