#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include"matrix_opt.h"
#define RND rand()%10

void vector_print(const char* s, gsl_vector * A)
{
	printf("%s\n",s);
	for(int i=0;i<A->size;i++)
	{
			printf("%8.3g\n",gsl_vector_get(A,i));
	}
	printf("\n");
}

void matrix_print(const char* s, gsl_matrix * A)
{
	printf("%s\n",s);
	for(int i=0;i<A->size1;i++)
	{
		for(int j=0;j<A->size2;j++)
		{
			printf("%.8g ",gsl_matrix_get(A,i,j));
		}
		printf("\n");
	}
	printf("\n");
}

void vector_rand(gsl_vector * A)
{
	for (int k = 0; k < A->size; k++)
	{
		gsl_vector_set(A,k,RND);
	}
}

void matrix_rand(gsl_matrix * A)
{
	for (int i = 0; i < A->size1; i++)
	{
		for (int k = 0; k < A->size2; k++)
		{
			gsl_matrix_set(A,i,k,RND);
		}
	}
}

void QR_GS_decompose(gsl_matrix * A, gsl_matrix * R)
{
	int m = A->size2;
	assert(A->size2==R->size1 && R->size2==R->size1);
	double R_ii, R_ij;

	for(int i=0;i<m;i++)
	{
		gsl_vector_view col_Ai = gsl_matrix_column(A,i); 
		R_ii = gsl_blas_dnrm2(&col_Ai.vector); //dot e with itself and take sqrt
		gsl_matrix_set(R,i,i,R_ii);
		gsl_vector_scale(&col_Ai.vector,1.0/R_ii); //normalization
		
		for(int j=i+1;j<m;j++)
		{
			gsl_vector_view col_Aj = gsl_matrix_column(A,j);
			gsl_blas_ddot(&col_Ai.vector,&col_Aj.vector,&R_ij); // prod of two vectors sent to dotprod
			gsl_matrix_set(R,i,j,R_ij); // putting into R
			gsl_blas_daxpy(-R_ij,&col_Ai.vector,&col_Aj.vector); //orthogonalization
			
		}
	}
}

void QR_GS_solve(gsl_matrix * Q, gsl_matrix * R, gsl_vector * b, gsl_vector * x)
{
	gsl_blas_dgemv(CblasTrans,1,Q,b,0,x);
	int m=R->size1;
	for(int i=m-1;i>=0;i--)
	{
		double s=0;
		for(int k=i+1;k<m;k++)
		{
		s+=gsl_matrix_get(R,i,k)*gsl_vector_get(x,k);
		}
		gsl_vector_set(x,i,(gsl_vector_get(x,i)-s)/gsl_matrix_get(R,i,i));
	}
}

void QR_GS_inverse(gsl_matrix * Q, gsl_matrix * R, gsl_matrix * B)
{
	gsl_vector* temp = gsl_vector_alloc(B->size2);
	gsl_matrix_set_identity(B);

	for(int i=0;i<B->size2;i++)
	{
		gsl_vector_view col_Bi=gsl_matrix_column(B,i);
		gsl_blas_dcopy(&col_Bi.vector,temp);
		QR_GS_solve(Q,R,temp,&col_Bi.vector);
	}
	gsl_vector_free(temp);
}

void QR_Givens(gsl_matrix * A)
{ /* A <− Q,R */
	for(int q=0; q<A->size2;q++)
	{
		for(int p=q+1;p<A->size1 || p<A->size2;p++)
		{
			double theta=atan2(gsl_matrix_get(A,p,q),gsl_matrix_get(A,q,q));
			for(int k=q;k<A->size2;k++)
			{
				double xq=gsl_matrix_get(A,q,k), xp=gsl_matrix_get(A,p,k);
				gsl_matrix_set(A,q,k,xq*cos(theta)+xp*sin(theta));
				gsl_matrix_set(A,p,k,-xq*sin(theta)+xp*cos(theta));
			}
			gsl_matrix_set(A,p,q,theta);
		}
	}

}

void QR_QTvec_Givens(gsl_matrix * QR, gsl_vector * v)
{
	for(int q=0; q<QR->size2;q++)
	{
		for(int p=q+1; p<QR->size1; p++)
		{
			double theta =gsl_matrix_get(QR,p,q);
			double vq = gsl_vector_get(v,q), vp=gsl_vector_get(v,p);
			gsl_vector_set(v,q,vq*cos(theta)+vp*sin(theta));
			gsl_vector_set(v,p,-vq*sin(theta)+vp*cos(theta));
		}
	}
}

void QR_Givens_solve(gsl_matrix * QR, gsl_vector * b)
{
	QR_QTvec_Givens(QR,b);
	for(int i=b->size-1; i>=0;i--)
	{
		double s=gsl_vector_get(b,i);
		for(int k = i+1;k<b->size;k++)
		{
			s-=gsl_matrix_get(QR,i,k)*gsl_vector_get(b,k);
			
		}
		gsl_vector_set(b,i,s/gsl_matrix_get(QR,i,i));
	}
}

void QR_Givens_unpack_Q(gsl_matrix * QR, gsl_matrix * Q)
{
	gsl_vector * ei = gsl_vector_alloc(QR->size1);
	for(int i=0; i<QR->size1;i++)
	{
		gsl_vector_set_basis(ei,i);
		QR_QTvec_Givens(QR,ei);
		for(int j=0;j<QR->size2; j++)
		{
			gsl_matrix_set(Q,i,j,gsl_vector_get(ei,j));
		}
		
	}
	gsl_vector_free(ei);
}

void QR_Givens_inverse(gsl_matrix *QR, gsl_matrix *invA) 
{
	int n = (*QR).size1, m = (*QR).size2;

	assert( n == m && invA->size1 == n && invA->size2 == m );

	for(int j = 0; j < m; j++) 
	{
		for(int i = 0; i < n; i++) 
		{
			gsl_matrix_set(invA,i,j, (i == j) ? 1 : 0 );
		}
		gsl_vector_view v = gsl_matrix_column(invA,j);
		QR_Givens_solve(QR,&v.vector);
	}
}