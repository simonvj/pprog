#ifndef HAVE_MATRIX_OPT_H
#define HAVE_MATRIX_OPT_H

void matrix_rand(gsl_matrix * A);
void vector_rand(gsl_vector * A);
void matrix_print(const char* s, gsl_matrix * A);
void vector_print(const char* s, gsl_vector * A);
void QR_GS_decompose(gsl_matrix * A, gsl_matrix * R);
void QR_GS_solve(gsl_matrix * Q, gsl_matrix * R, gsl_vector * b, gsl_vector * x);
void QR_GS_inverse(gsl_matrix * Q, gsl_matrix * R, gsl_matrix * B);
void QR_QTvec_Givens(gsl_matrix * QR, gsl_vector * v);
void QR_Givens_solve(gsl_matrix * QR, gsl_vector * b);
void QR_Givens(gsl_matrix * A);
void QR_Givens_unpack_Q(gsl_matrix * QR, gsl_matrix * Q);
void QR_Givens_inverse(gsl_matrix *QR, gsl_matrix *invA);

#endif