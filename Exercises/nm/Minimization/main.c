#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"minimization.h"
#include"user_functions.h"
#include"../Linear_Equations/matrix_opt.h"
#define RND rand()%5

int main()
{
	// Initialize system
	int n = 2, iterations;
	gsl_vector *x = gsl_vector_alloc(n);
	gsl_vector *dfdx = gsl_vector_alloc(n);
	double eps = 1e-6;

//--------------PART A---------------------

	// Rosenbrock
	gsl_vector_set(x,0,5);
	gsl_vector_set(x,1,-7);

	iterations = newton(&rosenbrock,&rosenbrockGradient,&rosenbrockHessian,x,eps);

	rosenbrockGradient(x,dfdx);

	// Print results
	printf("#----------------------Rosenbrock Newton---------------------\n#minimum x=%g, y=%g\n",gsl_vector_get(x,0),gsl_vector_get(x,1));
	printf("#f(x) = %g\n",rosenbrock(x));
	printf("#|df(x)| = %g\n",gsl_blas_dnrm2(dfdx));
	printf("#Did %d iterations.\n\n",iterations);

	// Himmelblau
	gsl_vector_set(x,0,3);
	gsl_vector_set(x,1,-1);

	iterations = newton(&himmelblau,&himmelblauGradient,&himmelblauHessian,x,eps);

	himmelblauGradient(x,dfdx);

	// Print results
	printf("#----------------------Himmelblau Newton---------------------\n#minimum x=%g, y=%g\n",gsl_vector_get(x,0),gsl_vector_get(x,1));
	printf("#f(x) = %g\n",himmelblau(x));
	printf("#|df(x)| = %g\n",gsl_blas_dnrm2(dfdx));
	printf("#Did %d iterations.\n\n",iterations);

//--------------PART B---------------------

		// Rosenbrock
	gsl_vector_set(x,0,5);
	gsl_vector_set(x,1,-7);

	iterations = quasiNewton(&rosenbrock,&rosenbrockGradient,x,eps);

	rosenbrockGradient(x,dfdx);

	// Print results
	printf("#----------------------Rosenbrock Quasi Newton(Broyden)---------------------\n#minimum x=%g, y=%g\n",gsl_vector_get(x,0),gsl_vector_get(x,1));
	printf("#f(x) = %g\n",rosenbrock(x));
	printf("#|df(x)| = %g\n",gsl_blas_dnrm2(dfdx));
	printf("#Did %d iterations.\n\n",iterations);

	// Himmelblau
	gsl_vector_set(x,0,3);
	gsl_vector_set(x,1,1);

	iterations = quasiNewton(&himmelblau,&himmelblauGradient,x,eps);

	himmelblauGradient(x,dfdx);

	// Print results
	printf("#----------------------Himmelblau Quasi Newton(Broyden)---------------------\n#minimum x=%g, y=%g\n",gsl_vector_get(x,0),gsl_vector_get(x,1));
	printf("#f(x) = %g\n",himmelblau(x));
	printf("#|df(x)| = %g\n",gsl_blas_dnrm2(dfdx));
	printf("#Did %d iterations.\n\n",iterations);

	// Root-finding
	printf("#Comparison can be found looking at data in Root-finding directory\n\n");

	// Fit exponential decay
	n=3;
	gsl_vector *xfit = gsl_vector_alloc(n);
	gsl_vector_set(xfit,0,5);
	gsl_vector_set(xfit,1,10);
	gsl_vector_set(xfit,2,0.5);

	quasiNewton(&expoDecayMin,&expoDecayGradient,xfit,eps);

	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	n = sizeof(t)/sizeof(t[0]);
	printf("# Fit to data with Quasi Newton\n");
	printf("# t \t y \t e\n");
	for(int i = 0; i < n; i++) 
	{
		printf("%g \t %g \t %g \n",t[i],y[i],e[i]);
	}

	printf("\n\n# t \t y \t with A = %.5g, T = %.5g, B = %.5g\n",
			gsl_vector_get(xfit,0),
			gsl_vector_get(xfit,1),
			gsl_vector_get(xfit,2));

	for(double t = 0; t < 10; t += 0.01) 
	{
		printf("%g \t %g\n",t,expoDecay(t,xfit));
	}
	printf("\n");

//--------------PART C---------------------
	int dim=2;
	n=dim+1;

	double convergence_goal = 1e-6; // largest distance between starting points

	gsl_matrix * simplex = gsl_matrix_alloc(dim,n);
	
	//------------- Himmelblau ---------------
	matrix_rand(simplex);

	matrix_print("#Himmelblau start Matrix=",simplex);

	iterations = DownhillSimplex(n,convergence_goal,&himmelblau,simplex);
	
	matrix_print("#Himmelblau end Matrix=",simplex);
	printf("iter=%i\n\n", iterations);

	//------------- Rosenbrock ---------------

	matrix_rand(simplex);

	matrix_print("#Rosenbrock start Matrix=",simplex);

	iterations = DownhillSimplex(n,convergence_goal,&rosenbrock,simplex);
	
	matrix_print("#Rosenbrock end Matrix=",simplex);
	printf("iter=%i\n\n", iterations);

	//------------- Exponential ---------------
	dim = 3, n=dim+1;
	gsl_matrix * simplex2 = gsl_matrix_alloc(dim,n);

	matrix_rand(simplex2);

	matrix_print("#Exponential start Matrix=",simplex2);

	iterations = DownhillSimplex(n,convergence_goal,&expoDecayMin,simplex2);
	
	matrix_print("#Exponential end Matrix=",simplex2);
	printf("iter=%i\n\n", iterations);

	// Free memory
	gsl_vector_free(x);
	gsl_vector_free(dfdx);
	gsl_vector_free(xfit);
	gsl_matrix_free(simplex);

	return 0;
}
