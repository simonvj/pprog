#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <assert.h>
#include <math.h>
#include "../Linear_Equations/matrix_opt.h"
#include "minimization.h"

// Routines 

double lineSearchBacktracking(
	double f(gsl_vector *x), 
	gsl_vector *x, 
	gsl_vector *gradf, 
	gsl_vector *dx,
	gsl_matrix * H,
	int updatetype
	
	)
{
	// 1 = Broyden update, 0 = regular update
	assert(updatetype==1 || updatetype==0);

	double lambda = 1, alpha = 1e-4, fx = f(x), deltaxdx;
	gsl_vector *xPlusLambdadx = gsl_vector_alloc((*x).size);
	gsl_vector *lambdadx = gsl_vector_alloc((*x).size);

	gsl_vector_memcpy(xPlusLambdadx,x);
	gsl_vector_memcpy(lambdadx,dx);
	gsl_vector_scale(lambdadx,lambda);
	gsl_vector_add(xPlusLambdadx,lambdadx);

	gsl_blas_ddot(dx,gradf,&deltaxdx);


	while( f(xPlusLambdadx) >= fx + alpha * lambda * deltaxdx )
	{
		lambda /= 2;
		gsl_vector_memcpy(xPlusLambdadx,x);
		gsl_vector_memcpy(lambdadx,dx);
		gsl_vector_scale(lambdadx,lambda);
		gsl_vector_add(xPlusLambdadx,lambdadx);
		if(updatetype==1)
		{
			if( lambda < 1e-6 ) 		
			{
			gsl_matrix_set_identity(H);
			break;
			}
		}
		else;
	}

	gsl_vector_free(xPlusLambdadx);
	gsl_vector_free(lambdadx);

	return lambda;
}

int newton(
	double f(gsl_vector *x),
	void gradient(gsl_vector *x, gsl_vector *dfdx),
	void hessian(gsl_vector *x, gsl_matrix *H),
	gsl_vector *x,
	double eps
	)
{
	int n = x->size;
	int iterations = 0;

	gsl_matrix *R = gsl_matrix_alloc(n,n);
	gsl_matrix *H = gsl_matrix_alloc(n,n);
	gsl_vector *dfdx = gsl_vector_alloc(n);
	gsl_vector *dx = gsl_vector_alloc(n);
	gsl_vector *temp = gsl_vector_alloc(n);

	gradient(x,dfdx);

	while( gsl_blas_dnrm2(dfdx) > eps ) 
	{
		iterations++;

		hessian(x,H);
		gsl_matrix_scale(H,-1.0);
		QR_GS_decompose(H,R);
		gsl_vector_memcpy(dx,dfdx);
		QR_GS_solve(H,R,dx,temp);

		double lambda = lineSearchBacktracking(f,x,dfdx,temp,H,0);

		gsl_vector_scale(temp,lambda);
		gsl_vector_add(x,temp);

		gradient(x,dfdx);
	}

	gsl_vector_free(dfdx);
	gsl_matrix_free(H);
	gsl_vector_free(dx);
	gsl_matrix_free(R);

	return iterations;
}

int quasiNewton(
	double f(gsl_vector *x),
	void gradient(gsl_vector *x, gsl_vector *dfdx),
	gsl_vector *x,
	double eps
	)
{
	int n = x->size;
	int iterations = 0;

	gsl_vector *dfdx = gsl_vector_alloc(n);
	gsl_vector *y = gsl_vector_alloc(n);
	gsl_vector *s = gsl_vector_alloc(n);
	gsl_matrix *H = gsl_matrix_alloc(n,n);
	gsl_vector *Hy = gsl_vector_alloc(n);
	double HyTs;
	gsl_matrix_set_identity(H);

	gradient(x,dfdx);

	while( gsl_blas_dnrm2(dfdx) > eps ) {
		iterations++;

		gsl_blas_dgemv(CblasNoTrans,-1.0,H,dfdx,0.0,s);

		double lambda = lineSearchBacktracking(f,x,dfdx,s,H,1);

		gsl_vector_scale(s,lambda);
		gsl_vector_add(x,s);
		gradient(x,y);
		gsl_vector_sub(y,dfdx);

		gsl_blas_dgemv(CblasNoTrans,1.0,H,y,0.0,Hy);
		gsl_blas_dgemv(CblasNoTrans,1.0,H,s,0.0,dfdx);
		gsl_blas_ddot(Hy,s,&HyTs);
		gsl_vector_sub(s,Hy);
		gsl_blas_dger(1.0/HyTs,s,dfdx,H);

		gradient(x,dfdx);
	}

	gsl_vector_free(dfdx);
	gsl_vector_free(y);
	gsl_vector_free(s);
	gsl_matrix_free(H);
	gsl_vector_free(Hy);

	return iterations;
}

// Downhill simplex

double convergence_test(gsl_matrix * simplex) // checking how close the points are to eachother
{
	double dist=0;
	int m=simplex->size1;
	int n=simplex->size2;

	for(int i=1; i<n; i++)
	{
		double dist_test=0;
		for(int j=0; j<m; j++)
		{
			dist_test+=pow(gsl_matrix_get(simplex,j,i)-gsl_matrix_get(simplex,j,0),2);
		}
		if(dist_test>dist)//testing each vector
		{
			dist=dist_test;
		}
	}
	return sqrt(dist);
}

void HiLow(
	gsl_matrix * simplex, 
	double f(gsl_vector * v),
	gsl_vector * hi,
	gsl_vector * low,
	int *indexHi,
	int *indexLow
)
{
	gsl_vector * test=gsl_vector_alloc(simplex->size1);
	for(int i=0; i<simplex->size2; i++)
	{
		for (int j = 0; j < simplex->size1; j++)
		{
			gsl_vector_set(test,j,gsl_matrix_get(simplex,j,i));
		}
		if(i==0)
		{
			gsl_vector_memcpy(hi,test);
			gsl_vector_memcpy(low,test);
			*indexHi=i;
			*indexLow=i;
		}
		else
		{
			if(f(test)>f(hi))
			{
				gsl_vector_memcpy(hi,test);
				*indexHi=i;
			}
			else if(f(test)<f(low))
			{
				gsl_vector_memcpy(low,test);
				*indexLow=i;
			}
		}
	}
	gsl_vector_free(test);
}

int DownhillSimplex(
	int n,
	double convergence_goal,
	double f(gsl_vector * v),
	gsl_matrix * simplex // starting points [p1, p2, p3, etc..]
)
{
	int m=simplex->size1;
	int iterations=0, indexHi=0, indexLow=0;
	gsl_vector * low=gsl_vector_alloc(m);
	gsl_vector * hi=gsl_vector_alloc(m);
	gsl_vector * cen=gsl_vector_alloc(m);
	gsl_vector * reflec=gsl_vector_alloc(m);
	gsl_vector * expan=gsl_vector_alloc(m);
	gsl_vector * contract=gsl_vector_alloc(m);
	gsl_vector * temp=gsl_vector_alloc(m);

	do
	{
		iterations++;

		HiLow(simplex, f, hi, low, &indexHi, &indexLow); // finds highest and lowest vector

		//finding centroid
		for(int row=0; row<m; row++)
		{	
			double temp=0;
			for (int column = 0; column < n; column++)
			{
				if (column!=indexHi)
				{
				temp+=gsl_matrix_get(simplex,row,column);	
				}
					
			}
			gsl_vector_set(cen, row, temp/m);
		}

		//making reflection
		gsl_vector_memcpy(reflec,hi);
		gsl_vector_scale(reflec,-1.0);
		gsl_vector_memcpy(temp, cen);
		gsl_vector_scale(temp, 2.0);
		gsl_vector_add(reflec,temp);

		if (f(reflec)<f(low))
		{
			gsl_vector_memcpy(expan,hi);
			gsl_vector_scale(expan,-2.0);
			gsl_vector_memcpy(temp, cen);
			gsl_vector_scale(temp, 3.0);
			gsl_vector_add(expan,temp);
			if(f(expan)<f(reflec))
			{
				for (int i = 0; i < m; i++)
				{
					gsl_matrix_set(simplex,i,indexHi,gsl_vector_get(expan,i));
				}
			}
			else
			{
				for (int i = 0; i < m; i++)
				{
					gsl_matrix_set(simplex,i,indexHi,gsl_vector_get(reflec,i));
				}
			}
		}
		else
		{
			if (f(reflec)<f(hi))
			{
				for (int i = 0; i < m; i++)
				{
					gsl_matrix_set(simplex,i,indexHi,gsl_vector_get(reflec,i));
				}
			}
			else
			{
				gsl_vector_memcpy(contract,hi);
				gsl_vector_memcpy(temp, cen);
				gsl_vector_add(contract,temp);
				gsl_vector_scale(contract, 0.5);
				if (f(contract)<f(hi))
				{
					for (int i = 0; i < m; i++)
					{
						gsl_matrix_set(simplex,i,indexHi,gsl_vector_get(contract,i));
					}
				}
				else
				{
					for (int i = 0; i < simplex->size2; i++)
					{
						if (i!=indexLow)
						{
							for (int j = 0; j < m; j++)
							{
								double s=0;
								s=0.5*(gsl_matrix_get(simplex,j,i)+gsl_matrix_get(simplex,j,indexLow));
								gsl_matrix_set(simplex,j,i,s);
							}
						}	
					}
				}
			}
		}
	}
	while
	(
		convergence_test(simplex) > convergence_goal 
	);

	gsl_vector_free(low);
	gsl_vector_free(hi);
	gsl_vector_free(cen);
	gsl_vector_free(contract);
	gsl_vector_free(reflec);
	gsl_vector_free(expan);
	gsl_vector_free(temp);

	return iterations;
}

