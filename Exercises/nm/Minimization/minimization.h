#ifndef HAVE_MINIMIZATION_H
#define HAVE_MINIMIZATION_H

double lineSearchBacktracking(
	double f(gsl_vector *x), 
	gsl_vector *x, 
	gsl_vector *gradf, 
	gsl_vector *dx,
	gsl_matrix * H,
	int updatetype
	
);
int newton(
	double f(gsl_vector *x),
	void gradient(gsl_vector *x, gsl_vector *dfdx),
	void hessian(gsl_vector *x, gsl_matrix *H),
	gsl_vector *x,
	double eps
);
int quasiNewton(
	double f(gsl_vector *x),
	void gradient(gsl_vector *x, gsl_vector *dfdx),
	gsl_vector *x,
	double eps
);
double convergence_test(gsl_matrix * simplex);
void HiLow(
	gsl_matrix * simplex, 
	double f(gsl_vector * v),
	gsl_vector * hi,
	gsl_vector * low,
	int *indexHi,
	int *indexLow
);
int DownhillSimplex(
	int n,
	double MinDist,
	double f(gsl_vector * v),
	gsl_matrix * simplex // starting points [p1, p2, p3, etc..]
);

#endif