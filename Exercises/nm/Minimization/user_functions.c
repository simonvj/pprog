#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>

// Functions to be used to get derivative and Hessian matrix

double rosenbrock(gsl_vector *x) 
{
	double x1 = gsl_vector_get(x,0);
	double y1 = gsl_vector_get(x,1);

	return pow(1-x1,2)+100*pow(y1-pow(x1,2),2);
}

void rosenbrockGradient(gsl_vector *x, gsl_vector* dfdx) 
{
	double x1 = gsl_vector_get(x,0);
	double y1 = gsl_vector_get(x,1);

	gsl_vector_set(dfdx,0,-2*(1-x1)-400*x1*(y1-pow(x1,2)));
	gsl_vector_set(dfdx,1,200*(y1-pow(x1,2)));
}

void rosenbrockHessian(gsl_vector *x, gsl_matrix *H) 
{
	double x1 = gsl_vector_get(x,0);
	double y1 = gsl_vector_get(x,1);

	gsl_matrix_set(H,0,0,2-400*y1+1200*pow(x1,2));
	gsl_matrix_set(H,1,1,200);
	gsl_matrix_set(H,0,1,-400*x1);
	gsl_matrix_set(H,1,0,-400*x1);
}

double himmelblau(gsl_vector *x) 
{
	double x1 = gsl_vector_get(x,0);
	double y1 = gsl_vector_get(x,1);

	return pow( pow(x1,2)+y1-11,2 ) + pow( x1+pow(y1,2)-7,2 );
}

void himmelblauGradient(gsl_vector *x, gsl_vector* dfdx) 
{
	double x1 = gsl_vector_get(x,0);
	double y1 = gsl_vector_get(x,1);

	gsl_vector_set(dfdx,0,4*x1*(pow(x1,2)+y1-11)+2*(x1+pow(y1,2)-7));
	gsl_vector_set(dfdx,1,2*(pow(x1,2)+y1-11)+4*y1*(x1+pow(y1,2)-7));
}

void himmelblauHessian(gsl_vector *x, gsl_matrix *H) 
{
	double x1 = gsl_vector_get(x,0);
	double y1 = gsl_vector_get(x,1);

	double H12And21 = 4*(x1+y1);
	gsl_matrix_set(H,0,0,4*y1+12*pow(x1,2)-42);
	gsl_matrix_set(H,1,1,4*x1+12*pow(y1,2)-28);
	gsl_matrix_set(H,0,1,H12And21);
	gsl_matrix_set(H,1,0,H12And21);
}

double expoDecay(double t, gsl_vector *params) 
{
	double A = gsl_vector_get(params,0);
	double T = gsl_vector_get(params,1);
	double B = gsl_vector_get(params,2);

	return A*exp(-t/T)+B;
}

void expoDecayGradient(gsl_vector *x, gsl_vector* df) {
	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);

	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int n = sizeof(t)/sizeof(t[0]);


	double result1 = 0, result2 = 0, result3 = 0, temp;
	for( int i = 0; i < n; i++) {
		temp = 2*(A*exp(-t[i]/T)+B-y[i])/pow(e[i],2);
		result1 += exp(-t[i]/T)*temp;
		result2 += A*t[i]/pow(T,2)*exp(-t[i]/T)*temp;
		result3 += temp;
	}

	gsl_vector_set(df,0,result1);
	gsl_vector_set(df,1,result2);
	gsl_vector_set(df,2,result3);
}

double expoDecayMin(gsl_vector *x) 
{
	double A = gsl_vector_get(x,0);
	double T = gsl_vector_get(x,1);
	double B = gsl_vector_get(x,2);

	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};

	int n = sizeof(t)/sizeof(t[0]);
	double result = 0;

	for( int i = 0; i < n; i++) {
		result += pow( (A*exp(-t[i]/T)+B-y[i])/e[i] , 2 );
	}

	return result;
}

double test(gsl_vector *x)
{
        double x1 = gsl_vector_get(x,0);
        double y1 = gsl_vector_get(x,1);

        return pow(x1+10,2)+pow(y1+10,2);
}