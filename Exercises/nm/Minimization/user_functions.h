#ifndef HAVE_USER_FUNCTIONS_H
#define HAVE_USER_FUNCTIONS_H

double rosenbrock(gsl_vector *x);
void rosenbrockGradient(gsl_vector *x, gsl_vector* dfdx);
void rosenbrockHessian(gsl_vector *x, gsl_matrix *H);
double himmelblau(gsl_vector *x);
void himmelblauGradient(gsl_vector *x, gsl_vector* dfdx);
void himmelblauHessian(gsl_vector *x, gsl_matrix *H);
double expoDecay(double t, gsl_vector *params);
double expoDecayMin(gsl_vector *x);
void expoDecayGradient(gsl_vector *x, gsl_vector* df);
double test(gsl_vector *x);

#endif