#include <stdio.h>
#include <math.h>
#include "montecarlo.h"

int main()
{
	// ------- Part A --------
	// Testing simple integrals
	int n = 3;
	double a[3] = {0, 0, 0};
	double b[3] = {2, 2, 2};

	double result, error;
	int N = 1e5;

	MonteCarlo(n,a,b,&f1mc,N,&result,&error);

	printf("#Int y²+x*z dxdydz from 0 to 2 for all\n");
	printf("#Result = %g\n",result);
	printf("#Error = %g\n",error);
	printf("#Should be %g\n\n",32./3+8);

	MonteCarlo(n,a,b,&f2mc,N,&result,&error);

	printf("#Int exp(-x²)*y*z dxdydz from 0 to 2 for all\n");
	printf("#Result = %g\n",result);
	printf("#Error = %g\n",error);
	printf("#Should be %g\n\n",1./8*sqrt(M_PI)*16*erf(2));

	// Hard integral
	double a1[3] = {0,0,0};
	double b1[3] = {M_PI,M_PI,M_PI};

	MonteCarlo(n,a1,b1,&f3mc,N,&result,&error);

	printf("#Int [1-cos(x)cos(y)cos(z)]^(-1)/(pi³) dxdydz from 0 to pi for all\n");
	printf("#Result = %.15g\n",result);
	printf("#Error = %.15g\n",error);
	printf("#Should be %.15g\n\n",pow(tgamma(1./4),4)/(4*pow(M_PI,3)));

	// -------- Part B ----------
	printf("#Testing error is proportional to O(1/sqrt(N))..\n");

	int N_max=1e6;

	printf("#Hard integral\n");

	for (int i = 1e2; i <= N_max; i*=10)
	{
		MonteCarlo(n,a1,b1,&f3mc,i,&result,&error);
		printf("%i %g %g\n", i, error, result);
	}

	printf("\n\n");
	printf("#Simple integral\n");

	for (int i = 1e2; i <= N_max; i*=10)
	{
		MonteCarlo(n,a,b,&f2mc,i,&result,&error);
		printf("%i %g %g\n", i, error, result);
	}

	// -------- Part C ---------
	printf("\n#2D integration with curves..\n");

	double a2=0, b2=4, acc=1e-6, eps=1e-6;

	MonteCarlo2DAdaptive(&fxy, &fc, &fd, a2, b2, acc, eps, &result);
	printf("#Result: %g\n", result);
	printf("#Exact result: %g\n", 11./6*pow(4,3));

	return 0;

}




