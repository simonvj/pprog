#include <math.h>
#include <stdlib.h>
#include "montecarlo.h"
#include"../Adaptive_Integration/integrator.h"

#define RND ((double)rand()/RAND_MAX)

double f1mc(double *x) 
{
	return x[1]*x[1]+x[0]*x[2];
}

double f2mc(double *x)
{
	return exp(-x[0]*x[0])*x[1]*x[2];
}

double f3mc(double *x)
{
	return 1./(pow(M_PI,3)*(1-cos(x[0])*cos(x[1])*cos(x[2])));
}

double fxy(double x, double y)
{
	return x*y;
}

double fc(double x)
{
	return sqrt(x);
}

double fd(double x)
{
	return 2*x;
}

void randomx(int n, double *a, double *b, double *x) 
{
	for(int i = 0; i < n; i++) 
	{
		x[i] = a[i] + RND*(b[i]-a[i]);
	}
}

// Multidimentional integrator
void MonteCarlo(
	int n,
	double *a,
	double *b,
	double f(double *x),
	int N,
	double *result,
	double *error
) 
{
	double volume = 1, sum = 0, sum2 = 0, x[n];

	for(int i = 0; i < n; i++) 
	{
		volume *= b[i]-a[i];
	}

	for(int i = 0; i < N; i++) 
	{
		randomx(n,a,b,x);
		double fx = f(x);
		sum += fx;
		sum2 += pow(fx,2);
	}

	double mean = sum/N;
	double sigma = sqrt(sum2/N - pow(mean,2));

	*result = mean*volume;
	*error = sigma/sqrt(N)*volume;
}

void MonteCarlo2DAdaptive(
	double f(double x, double y),
	double c_f(double x),
	double d_f(double x), 
	double a_old,
	double b_old,
	double acc,
	double eps,
	double *result
)
{
	double a, b, err;

	double f_outer(double x) 
	{
				double f_inner(double y)
				{
					return f(x,y);
				}
				a=c_f(x); 
				b=d_f(x);
				return adaptiveIntegratorOpenWithInf(f_inner,a,b,acc,eps,&err);
	}

	a=a_old; b=b_old;
	
	*result = adaptiveIntegratorOpenWithInf(f_outer,a,b,acc,eps,&err);
}