#ifndef HAVE_MONTECARLO_H
#define HAVE_MONTECARLO_H

double f1mc(double *x);
double f2mc(double *x);
double f3mc(double *x);
double fxy(double x, double y);
double fc(double x);
double fd(double x);
void randomx(int n, double *a, double *b, double *x);
void MonteCarlo(
	int n,
	double *a,
	double *b,
	double f(double *x),
	int N,
	double *result,
	double *error
); 
void MonteCarlo2DAdaptive(
	double f(double x, double y),
	double c_f(double x),
	double d_f(double x), 
	double a_old,
	double b_old,
	double acc,
	double eps,
	double *result
);

#endif