#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <gsl/gsl_rng.h>
#include "montecarlo.h"

double f3mc(double *x)
{
	return 1./(pow(M_PI,3)*(1-cos(x[0])*cos(x[1])*cos(x[2])));
}

int main()
{
	struct timeval t1,t2;
	//double elapsedTime;

	int n = 3;
	double result, error;
	int N = 1e6;


	// Hard integral
	double a[3] = {0,0,0};
	double b[3] = {M_PI,M_PI,M_PI};

//------------------ Monte carlo without multiprocessing ----------------

	printf("#Without multiprocessing\n");

	gettimeofday(&t1, NULL);
	MonteCarlo(n,a,b,&f3mc,N,&result,&error);
	gettimeofday(&t2, NULL);

	long elapsedTime = (t2.tv_sec-t1.tv_sec)*1000000 + t2.tv_usec-t1.tv_usec;

	printf("#Int [1-cos(x)cos(y)cos(z)]^(-1)/(pi³) dxdydz from 0 to pi for all\n");
	printf("#Result = %.15g\n",result);
	printf("#Error = %.15g\n",error);
	printf("#Should be %.15g\n\n",pow(tgamma(1./4),4)/(4*pow(M_PI,3)));
	printf("#Time spent %lo microseconds\n\n",elapsedTime);

//------------------ Monte carlo with multiprocessing ----------------
	
	printf("#With multiprocessing - using 3 cores\n");

	gettimeofday(&t1, NULL);
	MonteCarloMultiprocessing(n,a,b,f3mc,N,&result,&error);
	gettimeofday(&t2, NULL);

	elapsedTime = (t2.tv_sec-t1.tv_sec)*1000000 + t2.tv_usec-t1.tv_usec;

	printf("#Int [1-cos(x)cos(y)cos(z)]^(-1)/(pi³) dxdydz from 0 to pi for all\n");
	printf("#Result = %.15g\n",result);
	printf("#Error = %.15g\n",error);
	printf("#Should be %.15g\n\n",pow(tgamma(1./4),4)/(4*pow(M_PI,3)));
	printf("#Time spent %lo microseconds\n\n",elapsedTime);
	printf("#3 cores makes it approximately 3 times faster!\n");

	return 0;
}




