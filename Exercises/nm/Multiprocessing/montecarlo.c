#include <math.h>
#include <stdlib.h>
#include <omp.h>
#include "montecarlo.h"
#include "../Adaptive_Integration/integrator.h"
#include <gsl/gsl_rng.h>
#define RND ((double)rand()/RAND_MAX)

void randomx(int n, double *a, double *b, double *x) 
{
	for(int i = 0; i < n; i++) 
	{
		x[i] = a[i] + RND*(b[i]-a[i]);
	}
}

void randomxMultithreading(int n, double *a, double *b, double *x, gsl_rng* r) 
{
	for(int i = 0; i < n; i++) 
	{
		x[i] = a[i] + gsl_rng_uniform(r)*(b[i]-a[i]);
	}
}

// Without multiprocessing
void MonteCarlo(
	int n,
	double *a,
	double *b,
	double f(double *x),
	int N,
	double *result,
	double *error
) 
{
	double volume = 1, sum = 0, sum2 = 0, x[n];

	for(int i = 0; i < n; i++) 
	{
		volume *= b[i]-a[i];
	}

	for(int i = 0; i < N; i++) 
	{
		randomx(n,a,b,x);
		double fx = f(x);
		sum += fx;
		sum2 += pow(fx,2);
	}

	double mean = sum/N;
	double sigma = sqrt(sum2/N - pow(mean,2));

	*result = mean*volume;
	*error = sigma/sqrt(N)*volume;
}

// With multiprocessing
void MonteCarloMultiprocessing(
	int n,
	double *a,
	double *b,
	double f(double *x),
	int N,
	double *result,
	double *error
) 
{
	double volume = 1, sum = 0, sum2 = 0, x[n];

	for(int i = 0; i < n; i++) 
	{
		volume *= b[i]-a[i];
	}
	double sum11=0,sum12=0,sum13=0,sum21=0,sum22=0,sum23=0;

	const gsl_rng_type *T;
	gsl_rng *r1,*r2,*r3;
	gsl_rng_env_setup();
	T = gsl_rng_default;
	r1 = gsl_rng_alloc(T);
	r2 = gsl_rng_alloc(T);
	r3 = gsl_rng_alloc(T);

	int i, j, k;


		#pragma omp parallel sections
		{
			#pragma omp section
			{
				printf("#Running 1st process..\n");
				for(j = 0; j < N/3; j++) 
				{
					
					randomxMultithreading(n,a,b,x,r1);
					double fx1 = f(x);
					sum11 += fx1;
					sum21 += pow(fx1,2);
				}
				printf("#Finished 1st process..\n");
			}
			#pragma omp section
			{
				printf("#Running 2nd process..\n");
				for(i = N/3; i < 2*N/3; i++) 
				{
					randomxMultithreading(n,a,b,x,r2);
					double fx2 = f(x);
					sum12 += fx2;
					sum22 += pow(fx2,2);
				}			
				printf("#Finished 2nd process..\n");
			}
			#pragma omp section
			{
				printf("#Running 3nd process..\n");
				for(k = 2*N/3; k < N; k++) 
				{
					randomxMultithreading(n,a,b,x,r3);
					double fx3 = f(x);
					sum13 += fx3;
					sum23 += pow(fx3,2);
				}			
				printf("#Finished 3nd process..\n");
			}
		}

	sum = sum11 + sum12 + sum13;
	sum2 = sum21 + sum22 + sum23;

	double mean = sum/N;
	double sigma = sqrt(sum2/N - pow(mean,2));

	*result = mean*volume;
	*error = sigma/sqrt(N)*volume;


}
