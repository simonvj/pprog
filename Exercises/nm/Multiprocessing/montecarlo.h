#ifndef HAVE_MONTECARLO_H
#define HAVE_MONTECARLO_H

void randomx(int n, double *a, double *b, double *x);
void MonteCarlo(
	int n,
	double *a,
	double *b,
	double f(double *x),
	int N,
	double *result,
	double *error
); 
void MonteCarloMultiprocessing(
	int n,
	double *a,
	double *b,
	double f(double *x),
	int N,
	double *result,
	double *error
);


#endif