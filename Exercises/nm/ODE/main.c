#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<gsl/gsl_matrix.h>
#include"ode.h"

int main()
{
	//---------------- Part A1 ---------------------

	//initialize dimension, stepsize and starting point
	int n = 2; 
	double dx = 0.05, x=0;
	//allocate room for vectors
	gsl_vector * yx = gsl_vector_alloc(n);
	gsl_vector * yx_plus_h =gsl_vector_alloc(n);
	gsl_vector * err = gsl_vector_alloc(n);
	//initial conditions
	gsl_vector_set(yx,0,1);
	gsl_vector_set(yx,1,0);

	// Runge kutta 4
	printf("#x dydx dy2dx2 err1 err2\n");
	printf("%g %g %g\n", x,gsl_vector_get(yx,0),gsl_vector_get(yx,1));
	for(; x<2*M_PI; x+=dx)
	{
		rkstep4(x,dx,yx,funcHarmOsc,yx_plus_h,err);
		gsl_vector_memcpy(yx,yx_plus_h);
		printf("%g %g %g %g %g\n",x, gsl_vector_get(yx,0), gsl_vector_get(yx,1), gsl_vector_get(err,0),gsl_vector_get(err,1));
	}

	// Runge kutta 12
	printf("\n\n");
	x=0;
	printf("#x dydx dy2dx2 err1 err2\n");
	printf("%g %g %g\n", x,gsl_vector_get(yx,0),gsl_vector_get(yx,1));
	for(; x<2*M_PI; x+=dx)
	{
		rkstep4(x,dx,yx,funcHarmOsc,yx_plus_h,err);
		gsl_vector_memcpy(yx,yx_plus_h);
		printf("%g %g %g %g %g\n",x, gsl_vector_get(yx,0), gsl_vector_get(yx,1), gsl_vector_get(err,0),gsl_vector_get(err,1));
	}

	// ------------- Part A2 -------------------------

	double a = 0, b = 2, h, abs = 1e-6, eps = 1e-6;

	gsl_vector_set(yx,0,1);
	gsl_vector_set(yx,1,0);
	h = copysign(0.1,b-a);
	driver(&a,b,&h,yx,abs,eps,&rkstep4,&funcHarmOsc);

	printf("\n#Solve y'' = -y for y(0) = 1 and y'(0)=0\n");
	printf("#y'(%g) = %g y(%g) = %g\n",a,gsl_vector_get(yx,0),a,gsl_vector_get(yx,1));
	printf("#Exact solution is %g %g\n",cos(a), -sin(a));

	// --------------- Part B ------------------
	n = 2;
	int maxSteps = 1e4, DRIVER_FAIL=0;
	a=0; b=M_PI;

	gsl_vector * tPath = gsl_vector_alloc(maxSteps);
	gsl_matrix * yPath = gsl_matrix_alloc(n,maxSteps);

	gsl_vector_set(tPath,0,a);
	gsl_matrix_set(yPath,0,0,1);
	gsl_matrix_set(yPath,0,1,0);
	h = copysign(0.01,b-a);

	int lastStep = driverPathStoring(tPath,b,&h,yPath,abs,eps,&rkstep4,&funcHarmOsc);

	if( lastStep == DRIVER_FAIL ) 
	{
		fprintf(stderr,"Driver couldn't find a solution within %d steps\n",maxSteps);
		return 0;
	}

	printf("# Solve Harmonic function again\n\n");
	printf("# t \t yODE \t yExact\n");
	for(int i = 0; i < lastStep; i++) 
	{
		printf("%g %g %g\n",gsl_vector_get(tPath,i),gsl_matrix_get(yPath,0,i),gsl_matrix_get(yPath,1,i));
		//printf("\t %.3g\n",1.0/(1+exp(-gsl_vector_get(tPath,i))));
	}

	//---------- Part C -------------

	//Made for 1D function
	a = 0, b = 2, abs = 1e-6, eps = 1e-6;
	gsl_vector * y = gsl_vector_alloc(1);
	gsl_vector_set(y,0,a);
	h = copysign(0.1,b-a);
	double I = ODEIntegratorRK4(a,b,h,y,abs,eps,&funcGaussWithSquare);

	printf("\n#Int x²*exp(-x²) dx form 0 to 2 = %.17g\n",I );
	printf("#Exact solution = 1/4*sqrt(pi)*erf(2)-1/(e⁴) = %.15g\n", 1./4*sqrt(M_PI)*erf(2)-1/pow(exp(1),4));

	gsl_vector_free(y);
	gsl_vector_free(tPath);
	gsl_matrix_free(yPath);
	gsl_vector_free(yx);
	gsl_vector_free(yx_plus_h);
	gsl_vector_free(err);

	return 0;
}
