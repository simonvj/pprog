#include<math.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"ode.h"
#include<stdio.h>

void func(double x, gsl_vector * y, gsl_vector * dydx)
{
	assert(y->size==2);
	// y_1'=3*y_1+sin(x)
	// y_2'=3*y_1+3*y_2
	gsl_vector_set(dydx,0,3*gsl_vector_get(y,0)+2*sin(x));
	gsl_vector_set(dydx,1,3*gsl_vector_get(y,0)+3*gsl_vector_get(y,1));
}

void funcHarmOsc(double x, gsl_vector * y, gsl_vector * dydx)
{
	assert(y->size==2);
	// Harmonic osc.. u''=-u -> y1=u, y2=u' -> y1'=y2, y2'=-y1
	gsl_vector_set(dydx,0,gsl_vector_get(y,1));
	gsl_vector_set(dydx,1,-gsl_vector_get(y,0));
}

void func1D(double x, gsl_vector * y, gsl_vector * dydx)
{
	assert(y->size==1);
	double y0=gsl_vector_get(y,0);
	gsl_vector_set(dydx,0,y0*(1-y0));
}

void funcGaussWithSquare(double x, gsl_vector * y, gsl_vector * dydx)
{
	assert(y->size==1);
	gsl_vector_set(dydx,0,x*x*exp(-x*x));
}

void rkstep12(
	double x, double h, gsl_vector * yx,
	void f(double x, gsl_vector * y, gsl_vector * dydx),
	gsl_vector * yx_plus_h, gsl_vector * err
)
{
	int n = yx -> size;
	gsl_vector * k = gsl_vector_alloc(n);
	gsl_vector * k0 = gsl_vector_alloc(n);
	gsl_vector * k1 = gsl_vector_alloc(n);
	gsl_vector * temp = gsl_vector_alloc(n);
	f(x,yx,k0);
	gsl_vector_memcpy(temp,k0);
	gsl_vector_scale(temp,h);// temp=h*k0
	gsl_vector_add(temp,yx); // temp = yx + h*k0
	f(x+h,temp,k1);
	gsl_vector_add(k1,k0);
	gsl_vector_scale(k1,0.5);
	gsl_vector_memcpy(k,k1);


	for (int i = 0; i < yx->size; i++)
	{
		double y=gsl_vector_get(yx,i);
		double yx_plus_h_star=gsl_vector_get(yx,i)+h*gsl_vector_get(k0,i);
		gsl_vector_set(yx_plus_h,i,y+h*gsl_vector_get(k,i));
		gsl_vector_set(err,i,gsl_vector_get(yx_plus_h,i)-yx_plus_h_star);
	}
	gsl_vector_free(k);
	gsl_vector_free(k0);
	gsl_vector_free(k1);
	gsl_vector_free(temp);

	//set yx_plus_h and err
}

void rkstep4(
	double x, double h, gsl_vector * yx,
	void f(double x, gsl_vector * y, gsl_vector * dydx),
	gsl_vector * yx_plus_h, gsl_vector * err
)
{
	int n = yx -> size;
	gsl_vector * k = gsl_vector_calloc(n);
	gsl_vector * k0 = gsl_vector_alloc(n);
	gsl_vector * k1 = gsl_vector_alloc(n);
	gsl_vector * k2 = gsl_vector_alloc(n);
	gsl_vector * k3 = gsl_vector_alloc(n);
	gsl_vector * temp = gsl_vector_alloc(n);
	// k0
	f(x,yx,k0);
	//k1
	gsl_vector_memcpy(temp,k0);
	gsl_vector_scale(temp,h/2);// temp=h*k0
	gsl_vector_add(temp,yx); // temp = yx + h*k0/2
	f(x+h/2,temp,k1);
	//k2
	gsl_vector_memcpy(temp,k1);
	gsl_vector_scale(temp,h/2);// temp=h*k1/2
	gsl_vector_add(temp,yx); // temp = yx + h*k1/2
	f(x+h/2,temp,k2);
	//k3
	gsl_vector_memcpy(temp,k2);
	gsl_vector_scale(temp,h);// temp=h*k2
	gsl_vector_add(temp,yx); // temp = yx + h*k2
	f(x+h,temp,k3);
	// making final k vector
	gsl_vector_memcpy(temp,k0);
	gsl_vector_scale(temp,1.0/6);
	gsl_vector_add(k,temp);

	gsl_vector_memcpy(temp,k1);
	gsl_vector_scale(temp,1.0/3);
	gsl_vector_add(k,temp);

	gsl_vector_memcpy(temp,k2);
	gsl_vector_scale(temp,1.0/3);
	gsl_vector_add(k,temp);

	gsl_vector_memcpy(temp,k3);
	gsl_vector_scale(temp,1.0/6);
	gsl_vector_add(k,temp);

	for (int i = 0; i < yx->size; i++)
	{
		double y=gsl_vector_get(yx,i);
		double yx_plus_h_star=y+h*(gsl_vector_get(k0,i)/6+4*gsl_vector_get(k1,i)/6+gsl_vector_get(k2,i)/6);
		gsl_vector_set(yx_plus_h,i,y+h*gsl_vector_get(k,i));
		gsl_vector_set(err,i,gsl_vector_get(yx_plus_h,i)-yx_plus_h_star);
	}
	gsl_vector_free(k);
	gsl_vector_free(k0);
	gsl_vector_free(k1);
	gsl_vector_free(k2);
	gsl_vector_free(k3);
	gsl_vector_free(temp);

	//set yx_plus_h and err
}

void driver(
	double *t,
	double b,
	double *h,
	gsl_vector *y,
	double abs,
	double eps,
	void stepper(
		double t, double h, gsl_vector *y,
		void f(double t, gsl_vector *y, gsl_vector *dydt),
		gsl_vector *yh, gsl_vector *err
		),
	void f(double t, gsl_vector *y, gsl_vector *dydt)
) 
{
	int n = y->size;
	double a = *t, tol, normErr;
	gsl_vector *yh = gsl_vector_alloc(n);
	gsl_vector *err = gsl_vector_alloc(n);

	while( *t < b ) 
	{
		if(*t+*h>b) 
		{
			*h = b-*t;
		}
		stepper(*t,*h,y,f,yh,err);

		tol = ( eps*gsl_blas_dnrm2(yh) + abs ) * sqrt(*h/(b-a));
		normErr = gsl_blas_dnrm2(err);

		if( normErr < tol ) 
		{
			*t += *h;
			gsl_vector_memcpy(y,yh);
		}

		*h *= pow(tol/normErr,0.25)*0.95; // eq. 40
	}

	gsl_vector_free(yh);
	gsl_vector_free(err);
}

int driverPathStoring(
	gsl_vector *tPath,
	double b,
	double *h,
	gsl_matrix *yPath,
	double abs,
	double eps,
	void stepper(
		double t, double h, gsl_vector *y,
		void f(double t, gsl_vector *y, gsl_vector *dydt),
		gsl_vector *yh, gsl_vector *err
		),
	void f(double t, gsl_vector *y, gsl_vector *dydt)
) 
{
	int n = yPath->size1;
	int maxSteps = (*tPath).size, step = 0, DRIVER_FAIL=0;
	double a = gsl_vector_get(tPath,0), tol, normErr, t;
	gsl_vector *yh = gsl_vector_alloc(n);
	gsl_vector *err = gsl_vector_alloc(n);
	gsl_vector_view y, yNext;

	while( gsl_vector_get(tPath,step) < b ) 
	{
		t = gsl_vector_get(tPath,step);
		y = gsl_matrix_column(yPath,step);

		if(t+*h>b) 
		{
			*h = b-t;
		}
		stepper(t,*h,&y.vector,f,yh,err);

		tol = ( eps*gsl_blas_dnrm2(yh) + abs ) * sqrt(*h/(b-a));
		normErr = gsl_blas_dnrm2(err);
		//printf("\n%g\n", normErr);

		if( normErr < tol ) 
		{
			step++;
			if( step+1 > maxSteps ) 
			{
				return DRIVER_FAIL;
			}
			gsl_vector_set(tPath,step,t+*h);
			yNext = gsl_matrix_column(yPath,step);
			gsl_vector_memcpy(&yNext.vector,yh);
		}

		*h *= pow(tol/normErr,0.25)*0.95;
	}

	gsl_vector_free(yh);
	gsl_vector_free(err);

	return step+1;
}

double ODEIntegratorRK4(
	double a,
	double b,
	double h,
	gsl_vector * y,
	double abs,
	double eps,
	void f(double x, gsl_vector *y, gsl_vector *dydx))
{
	//Made for a 1D function
	driver(&a,b,&h,y,abs,eps,&rkstep4,f);
	double I = gsl_vector_get(y,0);
	return I;

}