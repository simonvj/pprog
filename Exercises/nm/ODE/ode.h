#ifndef HAVE_ODE_H
#define HAVE_ODE_H
#include<gsl/gsl_vector.h>

void func(double x, gsl_vector * y, gsl_vector * dydx);
void funcHarmOsc(double x, gsl_vector * y, gsl_vector * dydx);
void func1D(double x, gsl_vector * y, gsl_vector * dydx);
void funcGaussWithSquare(double x, gsl_vector * y, gsl_vector * dydx);
void rkstep12(
	double x, double h, gsl_vector * yx,
	void f(double x, gsl_vector * y, gsl_vector * dydt), 
	gsl_vector * yx_plus_h, gsl_vector * err
);
void rkstep4(
	double x, double h, gsl_vector * yx,
	void f(double x, gsl_vector * y, gsl_vector * dydx),
	gsl_vector * yx_plus_h, gsl_vector * err
);
void driver(
	double *t,
	double b,
	double *h,
	gsl_vector *y,
	double abs,
	double eps,
	void stepper(
		double t, double h, gsl_vector *y,
		void f(double t, gsl_vector *y, gsl_vector *dydt),
		gsl_vector *yh, gsl_vector *err
		),
	void f(double t, gsl_vector *y, gsl_vector *dydt)
);

int driverPathStoring(
	gsl_vector *tPath,
	double b,
	double *h,
	gsl_matrix *yPath,
	double abs,
	double eps,
	void stepper(
		double t, double h, gsl_vector *y,
		void f(double t, gsl_vector *y, gsl_vector *dydt),
		gsl_vector *yh, gsl_vector *err
		),
	void f(double t, gsl_vector *y, gsl_vector *dydt)
);
double ODEIntegratorRK4(
	double x,
	double b,
	double h,
	gsl_vector * y,
	double abs,
	double eps,
	void f(double x, gsl_vector * y, gsl_vector * dydx)
);

#endif