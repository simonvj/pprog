#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<math.h>
#include"newtonMethods.h"

int main(void) {
	// ------------------- Part A -------------------------------

	printf("#Part A\n\n");

	// ------------------ fRandom -------------------------------
	int NumbOfVar = 2;
	double dx = 1e-4, epsilon = 1e-6;
	gsl_vector *xStart = gsl_vector_alloc(NumbOfVar);
	gsl_vector *fx = gsl_vector_alloc(xStart->size);
	gsl_vector_set(xStart,0,1);
	gsl_vector_set(xStart,1,0.5);

	fRandom(xStart,fx);
	printf("#Random function\n#xStart = {%g %g},",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1));
	printf(" f(x) = {%g %g}\n",gsl_vector_get(fx,0), gsl_vector_get(fx,1));

	int iterations = newton(fRandom,xStart,dx,epsilon);

	fRandom(xStart,fx);
	printf("%g %g %g %g\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1));
	printf("#xEnd={%g %g}, df(x)/dx={%g %g}\n#Did %d iterations.\n\n\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1),iterations);

	// -------------------- fRosenbrock ------------------------
	gsl_vector_set(xStart,0,3);
	gsl_vector_set(xStart,1,7);

	fRosenbrock(xStart,fx);
	printf("#Rosenbrock function\n#xStart = {%g %g},",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1));
	printf(" f(x) = {%g %g}\n",gsl_vector_get(fx,0), gsl_vector_get(fx,1));

	iterations = newton(fRosenbrock,xStart,dx,epsilon);

	fRosenbrock(xStart,fx);
	printf("%g %g %g %g\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1));
	printf("#xEnd={%g %g}, df(x)/dx={%g %g}\n#Did %d iterations.\n\n\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1),iterations);

	// ------------------- fHimmelblau ----------------------------------
	gsl_vector_set(xStart,0,3);
	gsl_vector_set(xStart,1,7);

	fHimmelblau(xStart,fx);
	printf("#fHimmelblau function\n#xStart = {%g %g},",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1));
	printf(" f(x) = {%g %g}\n",gsl_vector_get(fx,0), gsl_vector_get(fx,1));

	iterations = newton(fHimmelblau,xStart,dx,epsilon);

	fHimmelblau(xStart,fx);
	printf("%g %g %g %g\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1));
	printf("#xEnd={%g %g}, df(x)/dx={%g %g}\n#Did %d iterations.\n\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1),iterations);

	// ------------------- Part B ---------------------------

	printf("#Part B\n\n");

	// ------------------ fRandom -------------------------------
	gsl_vector_set(xStart,0,1);
	gsl_vector_set(xStart,1,0.5);

	fRandom(xStart,fx);
	printf("#Random function\n#xStart = {%g %g},",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1));
	printf(" f(x) = {%g %g}\n",gsl_vector_get(fx,0), gsl_vector_get(fx,1));

	iterations = newton_jacobian(fRandom_jacobian,xStart,dx,epsilon);

	fRandom(xStart,fx);
	printf("%g %g %g %g\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1));
	printf("#xEnd={%g %g}, df(x)/dx={%g %g}\n#Did %d iterations.\n\n\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1),iterations);

	// -------------------- fRosenbrock ------------------------
	gsl_vector_set(xStart,0,3);
	gsl_vector_set(xStart,1,7);

	fRosenbrock(xStart,fx);
	printf("#Rosenbrock function\n#xStart = {%g %g},",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1));
	printf(" f(x) = {%g %g}\n",gsl_vector_get(fx,0), gsl_vector_get(fx,1));

	iterations = newton_jacobian(fRosenbrock_jacobian,xStart,dx,epsilon);

	fRosenbrock(xStart,fx);
	printf("%g %g %g %g\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1));
	printf("#xEnd={%g %g}, df(x)/dx={%g %g}\n#Did %d iterations.\n\n\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1),iterations);

	// ------------------- fHimmelblau ----------------------------------
	gsl_vector_set(xStart,0,3);
	gsl_vector_set(xStart,1,7);

	fHimmelblau(xStart,fx);
	printf("#fHimmelblau function\n#xStart = {%g %g},",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1));
	printf(" f(x) = {%g %g}\n",gsl_vector_get(fx,0), gsl_vector_get(fx,1));

	iterations = newton_jacobian(fHimmelblau_jacobian,xStart,dx,epsilon);

	fHimmelblau(xStart,fx);
	printf("%g %g %g %g\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1));
	printf("#xEnd={%g %g}, df(x)/dx={%g %g}\n#Did %d iterations.\n\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1),iterations);

	/*
	// ------------------- GSL-root-finder -----------------------------
	printf("GSL-root-finder - Rosenbrock\n");
	gsl_vector_set(xStart,0,3); //setting start x value
	gsl_vector_set(xStart,1,7); //setting start y value
	const gsl_multiroot_fsolver_type *T;
 	gsl_multiroot_fsolver *s;

  	int status;
  	size_t iterations_gsl=0;

  	const size_t n = 2;
 	struct rparams p = {1.0, 100.0};
  	gsl_multiroot_function f = {&rosenbrock_f, n, &p};

  	T = gsl_multiroot_fsolver_hybrids;
  	s = gsl_multiroot_fsolver_alloc (T, 2);
  	gsl_multiroot_fsolver_set (s, &f, xStart);

  	do
    {
      	iterations_gsl++;
      	status = gsl_multiroot_fsolver_iterate (s);
      	if (status)   break;
      	status = gsl_multiroot_test_residual (s->f, epsilon);
    }
  	while (status == GSL_CONTINUE && iterations_gsl < 1000);

  	printf ("status = %s \niterations=%zu\n ", gsl_strerror (status),iterations_gsl);



  	gsl_multiroot_fsolver_free (s);
  	*/

  	// --------- Part C ---------

  	printf("#Part C\n\n");

  	// -------------------- fRosenbrock ------------------------

  	gsl_vector_set(xStart,0,3);
	gsl_vector_set(xStart,1,7);

	fRosenbrock(xStart,fx);
	printf("#Rosenbrock function\n#xStart = {%g %g},",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1));
	printf(" f(x) = {%g %g}\n",gsl_vector_get(fx,0), gsl_vector_get(fx,1));

	iterations = newton_jacobian_refined(fRosenbrock_jacobian,xStart,dx,epsilon);

	fRosenbrock(xStart,fx);
	printf("%g %g %g %g\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1));
	printf("#xEnd={%g %g}, df(x)/dx={%g %g}\n#Did %d iterations.\n\n\n",gsl_vector_get(xStart,0), gsl_vector_get(xStart,1),gsl_vector_get(fx,0), gsl_vector_get(fx,1),iterations);
	
	gsl_vector_free(xStart);
	gsl_vector_free(fx);

	return 0;
}