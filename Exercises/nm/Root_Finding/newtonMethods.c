#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_multiroots.h>
#include"../Linear_Equations/matrix_opt.h"
#include"newtonMethods.h"

int rosenbrock_f (const gsl_vector * x, void *params, gsl_vector * f)
{
  double a = ((struct rparams *) params)->a;
  double b = ((struct rparams *) params)->b;

  const double x0 = gsl_vector_get (x, 0);
  const double x1 = gsl_vector_get (x, 1);

  const double y0 = -2*(a-x0)-4*b*x0*(x1-x0*x0);
  const double y1 = 2*b*(x1-x0*x0);

  gsl_vector_set (f, 0, y0);
  gsl_vector_set (f, 1, y1);

  return GSL_SUCCESS;
}

void fRandom(gsl_vector * xVec, gsl_vector * fx) 
{
	double x = gsl_vector_get(xVec,0), y = gsl_vector_get(xVec,1);
	double A = 1e4;

	gsl_vector_set(fx,0, A*x*y - 1 );
	gsl_vector_set(fx,1, exp(-x) + exp(-y) - 1 - 1.0/A);
}

void fRandom_jacobian(gsl_vector * xVec, gsl_vector * fx, gsl_matrix * J) 
{
	double x = gsl_vector_get(xVec,0), y = gsl_vector_get(xVec,1);
	double A = 1e4;

	gsl_vector_set(fx,0, A*x*y - 1 );
	gsl_vector_set(fx,1, exp(-x) + exp(-y) - 1 - 1.0/A);
	gsl_matrix_set(J, 0, 0, A*y);
	gsl_matrix_set(J, 0, 1, A*x);
	gsl_matrix_set(J, 1, 0, -exp(-x));
	gsl_matrix_set(J, 1, 1, -exp(-y));
}

void fRosenbrock(gsl_vector * xVec, gsl_vector * fx) 
{
	double x = gsl_vector_get(xVec,0),y = gsl_vector_get(xVec,1);
	double a = 1, b=100;

	gsl_vector_set(fx,0,-2*(a-x)-4*b*x*(y-x*x));
	gsl_vector_set(fx,1,2*b*(y-x*x));
}

void fRosenbrock_jacobian(gsl_vector * xVec, gsl_vector * fx, gsl_matrix * J) 
{
	double x = gsl_vector_get(xVec,0),y = gsl_vector_get(xVec,1);
	double a = 1, b=100;

	gsl_vector_set(fx,0,-2*(a-x)-4*b*x*(y-x*x));
	gsl_vector_set(fx,1,2*b*(y-x*x));
	gsl_matrix_set(J, 0, 0, 2 - 4*b*y + 4*b*3*x*x);
	gsl_matrix_set(J, 0, 1, -4*b*x);
	gsl_matrix_set(J, 1, 0, -4*b*x);
	gsl_matrix_set(J, 1, 1, 2*b);
}

void fHimmelblau(gsl_vector * xVec, gsl_vector * fx) 
{
	double x = gsl_vector_get(xVec,0), y = gsl_vector_get(xVec,1);

	gsl_vector_set(fx,0, 4*x*(x*x+y-11) + 2*(x+y*y-7) );
	gsl_vector_set(fx,1, 2*(x*x+y-11) + 4*y*(x+y*y-7) );
}

void fHimmelblau_jacobian(gsl_vector * xVec, gsl_vector * fx, gsl_matrix * J) 
{
	double x = gsl_vector_get(xVec,0), y = gsl_vector_get(xVec,1);

	gsl_vector_set(fx,0, 4*x*(x*x+y-11) + 2*(x+y*y-7) );
	gsl_vector_set(fx,1, 2*(x*x+y-11) + 4*y*(x+y*y-7) );
	gsl_matrix_set(J, 0, 0, 2*(4*x*x + 2*(x*x +y -11) +1));
	gsl_matrix_set(J, 0, 1, 2*(2*x+2*y));
	gsl_matrix_set(J, 1, 0, 2*(2*x+2*y));
	gsl_matrix_set(J, 1, 1, 2*(4*y*y + 2*(y*y + x - 7) +1));
}

int newton(void f(gsl_vector * x,gsl_vector * fx), gsl_vector * x, double dx, double eps){
	int n=x->size, count=0;
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* z  = gsl_vector_alloc(n);
	gsl_vector* fz = gsl_vector_alloc(n);
	gsl_vector* df = gsl_vector_alloc(n);
	gsl_vector* Dx = gsl_vector_alloc(n);

	double lambda=1;
	do
	{
		count++;
		f(x,fx);
		for (int j=0;j<n;j++)
		{

			gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);
			f(x,df);
			gsl_vector_sub(df,fx); /* df=f(x+dx)-f(x) */
			for(int i=0;i<n;i++) gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx);
			gsl_vector_set(x,j,gsl_vector_get(x,j)-dx);
		}
		QR_GS_decompose(J,R);
		QR_GS_solve(J,R,fx,Dx);
		gsl_vector_scale(Dx,-1);

		while(1){
			count++;
			gsl_vector_memcpy(z,x);
			gsl_vector_add(z,Dx);
			f(z,fz);
			if( gsl_blas_dnrm2(fz)<(1-lambda/2)*gsl_blas_dnrm2(fx) || lambda<0.02 ) break;
			lambda*=0.5;
			gsl_vector_scale(Dx,0.5);
			}
		gsl_vector_memcpy(x,z);
		gsl_vector_memcpy(fx,fz);
	}
	while(gsl_blas_dnrm2(Dx)>dx || gsl_blas_dnrm2(fx)>eps);
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(fx);
	gsl_vector_free(z);
	gsl_vector_free(fz);
	gsl_vector_free(df);
	gsl_vector_free(Dx);
	return count;
}

int newton_jacobian(
	void f(
		gsl_vector* x, 
		gsl_vector* fx, 
		gsl_matrix* J), 
	gsl_vector* xstart, 
	double dx, 
	double tol)
{	
	int n = xstart->size;

	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* df_k = gsl_vector_alloc(n);
	gsl_vector* delta_x = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* f_delta_x = gsl_vector_alloc(n);

	double lambda;
	int count = 0;
	
	do
	{
		count++;
		f(xstart, fx, J);

		QR_GS_decompose(J,R);
		gsl_vector_scale(fx, -1.0);
		QR_GS_solve(J,R,fx,delta_x);
		gsl_vector_scale(fx, -1.0);

		lambda = 1.0;
		gsl_vector_add(xstart, delta_x);

		f(xstart, f_delta_x, J);

		
		while(gsl_blas_dnrm2(f_delta_x) > (1-lambda/2.0)*gsl_blas_dnrm2(fx) && lambda > 1.0/128)
		{
			count++;
			lambda /= 2.0;
			gsl_vector_scale(delta_x, lambda);
			gsl_vector_add(xstart, delta_x);
			f(xstart, f_delta_x, J);
		}

	}
	while(gsl_blas_dnrm2(fx) > tol && count < 1e6);

	gsl_vector_free(fx);
	gsl_vector_free(df_k);
	gsl_vector_free(delta_x);	
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(f_delta_x);

	return count;

}

int newton_jacobian_refined(
	void f(
		gsl_vector* x, 
		gsl_vector* fx, 
		gsl_matrix* J), 
	gsl_vector* xstart, 
	double dx, 
	double tol)
{	
	int n = xstart->size;

	gsl_vector* fx = gsl_vector_alloc(n);
	gsl_vector* df_k = gsl_vector_alloc(n);
	gsl_vector* delta_x = gsl_vector_alloc(n);
	gsl_matrix* J = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* f_delta_x = gsl_vector_alloc(n);

	double lambda;
	int count = 0;
	
	do
	{
		count++;
		f(xstart, fx, J);

		QR_GS_decompose(J,R);
		gsl_vector_scale(fx, -1.0);
		QR_GS_solve(J,R,fx,delta_x);
		gsl_vector_scale(fx, -1.0);

		lambda = 1.0;
		gsl_vector_add(xstart, delta_x);

		f(xstart, f_delta_x, J);

		// implementing refined linesearch
		double temp=0;
		gsl_blas_ddot(fx,fx,&temp);
		double g0 = 0.5*temp;
		double gprime0 = -temp;
		double glambda = 0;

		while(gsl_blas_dnrm2(f_delta_x) > (1-lambda/2.0)*gsl_blas_dnrm2(fx) && lambda > 1.0/128)
		{	
			count++;
			double c = (glambda-g0-gprime0*lambda)/(lambda*lambda);
			glambda = g0+gprime0*lambda+c*lambda*lambda;
			gsl_vector_scale(delta_x, lambda);
			gsl_vector_add(xstart, delta_x);
			f(xstart, f_delta_x, J);
			lambda = gprime0/(2*c);

		}

	}
	while(gsl_blas_dnrm2(fx) > tol && count < 1e6);

	gsl_vector_free(fx);
	gsl_vector_free(df_k);
	gsl_vector_free(delta_x);	
	gsl_matrix_free(J);
	gsl_matrix_free(R);
	gsl_vector_free(f_delta_x);

	return count;

}