#ifndef HAVE_NEWTONMETHODS_H

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

void fRandom(gsl_vector * xVec, gsl_vector * fx);
void fRosenbrock(gsl_vector * xVec, gsl_vector * fx);
void fHimmelblau(gsl_vector * xVec, gsl_vector * fx);
void fRandom_jacobian(gsl_vector * xVec, gsl_vector * fx, gsl_matrix * J);
void fRosenbrock_jacobian(gsl_vector * xVec, gsl_vector * fx, gsl_matrix * J);
void fHimmelblau_jacobian(gsl_vector * xVec, gsl_vector * fx, gsl_matrix * J);
int newton(void f(gsl_vector * x,gsl_vector * fx), gsl_vector * x, double dx, double eps);
int newton_jacobian(
	void f(
		gsl_vector * x,
		gsl_vector * fx,
		gsl_matrix * J), 
	gsl_vector * x, 
	double dx, 
	double eps);
int newton_jacobian_refined(
	void f(
		gsl_vector* x, 
		gsl_vector* fx, 
		gsl_matrix* J), 
	gsl_vector* xstart, 
	double dx, 
	double tol);
// gsl-comparison
struct rparams{double a;double b;};
int rosenbrock_f (const gsl_vector * x, void *params, gsl_vector * f);

#define HAVE_NEWTONMETHODS_H
#endif