#include<stdlib.h>
#include<assert.h>
#include<stdio.h>
#include"cspline.h"

cspline* cspline_alloc(int n, double* x, double* y)
{ 
cspline* s=(cspline*)malloc(sizeof(cspline));
s->b = (double *)malloc((n-1)* sizeof(double));
s->c = (double *)malloc((n-1)* sizeof(double));
s->d = (double *)malloc((n-1)* sizeof(double));
s->x = (double *)malloc((n)* sizeof(double)); 
s->y = (double *)malloc((n)* sizeof(double)); 
s->n = n ; 

int i; 

for (i =0; i<n ; i ++)
{
	s->x[i]=x[i]; 
	s->y[i]=y[i];
}

double p[n-1], dx[n-1], D[n], Q[n-1], B[n];

for(i=0; i<n-1; i++)
{
	dx[i]=x[i+1]-x[i];
	p[i]=(y[i+1]-y[i])/dx[i];
}

D[0]=2; Q[0]=1;// arbitrary starting choice

for(i=0; i<n-2; i++)
{
	D[i+1]=2*dx[i]/dx[i+1]+2;
	Q[i+1]=dx[i]/dx[i+1];
	B[i+1]=3*(p[i]+p[i+1]*dx[i]/dx[i+1]);
}

D[n-1]=2; B[0]=3*p[0]; B[n-1]=3*p[n-2];

for(i=1; i < n; i++)
{
	D[i] -= Q[i-1]/D[i-1];
	B[i] -= B[i-1]/D[i-1];
}

s->b[n-1] = B[n-1]/D[n-1];

for(i=n-2; i >= 0; i--) 
{
	s->b[i] = (B[i] - Q[i] * s->b[i+1])/D[i];
}
for (i = 0; i < n-1; i++)
{
	s->c[i] = (-2*s->b[i] - s->b[i+1] +3*p[i])/dx[i];
	s->d[i] = (s->b[i] + s->b[i+1] - 2*p[i])/dx[i]/dx[i];
}

return s;

}


double cspline_eval(cspline * s, double z)
{
	assert(z >= s->x[0] && z <= s->x[s->n - 1]);
	int i = 0, j = s->n - 1;	// binary search for the interval for z :
	while (j - i > 1) {
		int m = (i + j) / 2;
		if (z > s->x[m])
			i = m;
		else
			j = m;
	}
	double dx = z - s->x[i];	// calculate the inerpolating spline :
	return s->y[i] + dx * (s->b[i] + dx * (s->c[i] + dx * s->d[i]));
}

double cspline_integ(cspline* s, double z)
{
	assert(z>=s->x[0]);
	int i=0, j=s->n-1; double dx=0, result=0;
	while (j-i >1){int m=( i+j )/2 ; if( z>s->x[m]) i=m; else j=m;}
	for (int u = 0; u < i; u++)
	{
		dx=s->x[u+1]-s->x[u];
		result+=dx*(s->y[u]+dx*(s->b[u]/2+dx*(s->c[u]/3+dx*s->d[u]/4)));
	}
	dx=z-s->x[i];
	result+=dx*(s->y[i]+dx*(s->b[i]/2+dx*(s->c[i]/3+dx*s->d[i]/4)));
	return result;
}

double cspline_dydx(cspline * s, double z)
{
	assert(z >= s->x[0] && z <= s->x[s->n - 1]);
	int i = 0, j = s->n - 1;	// binary search for the interval for z :
	while (j - i > 1) {
		int m = (i + j) / 2;
		if (z > s->x[m])
			i = m;
		else
			j = m;
	}
	double dx = z - s->x[i];	// calculate the inerpolating spline :
	return s->b[i] + dx * (2*s->c[i] + 3*dx * s->d[i]);
}

void cspline_free(cspline * s)
{				//free the allocated memory
	free(s->x);
	free(s->y);
	free(s->b);
	free(s->c);
	free(s->d);
	free(s);
}