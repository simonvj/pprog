#include<assert.h>
//creates linear spline from data x and y at point z
double linspline(int n, double *x, double *y, double z)
{
	assert(n>1 && z>=x[0] && z<=x[n-1]); // checking z does not extrapolate and x and y are not just a number
	int i=0, j=n-1;
	while(j-i>1)
	{ 
	int m=(i+j)/2 ;
	if(z>x[m]) i=m;
	else j=m; 
	}
	return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);
}

double linspline_integ(int n, double *x, double *y, double z)
{
	assert(n>1 && z>=x[0] && z<=x[n-1]); // checking z does not extrapolate and x and y are not just a number
	int i=0, j=n-1;
	double result=0;
	while(j-i>1)
	{ 
	int m=(i+j)/2 ;
	if(z>x[m]) i=m;
	else j=m; 
	}
	for (int u = 0; u < i; u++)
	{
		double a=(y[u+1]-y[u])/(x[u+1]-x[u]), c=y[u]-a*x[u];
		result+=a/2*(x[u+1]*x[u+1]-x[u]*x[u])+c*(x[u+1]-x[u]);
	}
	double a=(y[i+1]-y[i])/(x[i+1]-x[i]), c=y[i]-a*x[i];

	result+=a*(z*z-x[i]*x[i])/2+c*(z-x[i]);
	return result;
}