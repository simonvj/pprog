#ifndef HAVE_LINSPLINE_H
#define HAVE_LINSPLINE_H
double linspline(int, double *, double *, double);
double linspline_integ(int, double*, double*, double);
#endif