#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"linspline.h"
#include"qspline.h"
#include"cspline.h"

int main()
{
	printf("#x\ty\n");

	double max=6.2, min=0, dx1=0.7, dx2=0.1;
	double *x=malloc((max-min)/dx1*sizeof(double)); 
	double *y=malloc((max-min)/dx1*sizeof(double)); 
	int count=0, n;
	// creates data points to spline
	for (double i = min; i < max; i+=dx1)
	{
		x[count]=i;
		y[count]=sin(i)*sin(i)*cos(2.0*i);
		printf("%g %g\n", x[count], y[count]);
		count++; 
	}n=count; count=0;
	// linear spline
	printf("\n\n#x_linspline\ty_linspline\tintegration_min_to_z\n");

	for (double i = x[0]; i<x[n-1]; i+=dx2) //x[n-1] is the max value we can interpolate to, n-1 because we start counting from 0
	{
		printf("%g %g %g\n", i, linspline(n, x, y, i), linspline_integ(n, x, y, i));
	}
	printf("%g\n", linspline_integ(n,x,y,3) );
	//quardratic spline
	printf("\n\n#x_qspline\ty_qspline\tint\tdydx\n");

	qspline* s=qspline_alloc(n,x,y);

	for (double i = x[0]; i <= x[n-1]; i+=dx2)
	{
		printf("%g %g %g %g\n", i, qspline_eval(s, i), qspline_integ(s, i), qspline_dydx(s, i) );
	}
	// cubic spline
	printf("\n\n#x_cspline\ty_cspline\tint\tdydx");

	qspline_free(s);
	cspline* S=cspline_alloc(n,x,y);

	for (double i = x[0]; i <= x[n-1]; i+=dx2)
	{
		printf("%g %g %g %g\n", i, cspline_eval(S, i), cspline_integ(S, i), cspline_dydx(S, i) );
	}

	cspline_free(S);
	free(x);
	free(y);
	return 0;
}
