#include<stdlib.h>
#include<assert.h>
#include<stdlib.h>
#include"qspline.h"

qspline* qspline_alloc(int n, double* x, double* y)
{ 
qspline* s=(qspline*)malloc(sizeof(qspline));
s->b = (double *)malloc((n-1)* sizeof(double));
s->c = (double *)malloc((n-1)* sizeof(double));
s->x = (double *)malloc((n)* sizeof(double)); 
s->y = (double *)malloc((n)* sizeof(double)); 
s->n = n ; 

for ( int i =0; i<n ; i ++)
{
	s->x[i]=x[i]; 
	s->y[i]=y[i];
}

int i; double p[n-1], dx[n-1]; 

for(i=0; i<n-1; i++)
{
	dx[i]=x[i+1]-x[i];
	p[i]=(y[i+1]-y[i])/dx[i];
}

s->c[0]=0; 

for(i=0; i<n-2; i++)
{
	s->c[i+1]=(p[i+1]-p[i]- s->c[i]*dx[i])/dx[i+1];
}

s->c[n-2]/=2;

for(i=n-3; i >=0;i--)
{
	s->c[i]=(p[i+1]-p[i]- s->c[i+1]*dx[i+1])/dx[i];
}

for(i=0; i<n-1; i++) 
{
	s -> b[i] = p[i]-s -> c[i]*dx[i];
}

return s;
}

double qspline_eval(qspline* s, double z)
{ 
assert(z>=s->x[0]);
int i=0, j=s->n-1;
while (j-i >1){int m=( i+j )/2 ; if( z>s->x[m]) i=m; else j=m;}
double dx = z - s->x[i];
return s->y[i] + dx*(s->b[i] + dx*s->c[i]); 
}

double qspline_dydx(qspline* s, double z)
{
	assert(z>=s->x[0]);
	assert(z<=s->x[s->n-1]);
	int i=0, j=s->n-1;
	while (j-i >1){int m=( i+j )/2 ; if( z>=s->x[m]) i=m; else j=m;}
	double dx = z - s->x[i];
	return s->b[i] + 2*s->c[i]*dx; 
}

double qspline_integ(qspline* s, double z)
{
	assert(z>=s->x[0]);
	int i=0, j=s->n-1; double dx=0, result=0;
	while (j-i >1){int m=( i+j )/2 ; if( z>s->x[m]) i=m; else j=m;}
	for (int u = 0; u < i; u++)
	{
		dx=s->x[u+1]-s->x[u];
		result+=dx*(s->y[u]+dx*(s->b[u]/2+dx*s->c[u]/3));
	}
	dx=z-s->x[i];
	result+=dx*(s->y[i]+dx*(s->b[i]/2+dx*s->c[i]/3));
	return result;
}

void qspline_free(qspline* s)
{ 
free( s->x ); free(s->y); free(s->b); free(s->c); free(s);
}
