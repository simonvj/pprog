#include<gsl/gsl_multimin.h>
#include<assert.h>

struct exp_dat{int n; double *t, *y, *e;};

double rad_func(const gsl_vector *r, void *params)
{
	double A=gsl_vector_get(r,0), Time=gsl_vector_get(r,1), B=gsl_vector_get(r,2);
	struct exp_dat *v = (struct exp_dat*) params;
	double *t=v->t, *y=v->y, *e=v->e, sum=0;
	int n=v->n;
	for(int i=0; i<n; i++) sum+=pow( ((A*exp(-t[i] / Time)+B)- y[i])/e[i], 2);
	return sum;
}

int radioactive()
{
	double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]), dim = 3;

	printf("# t[i], y[i], e[i]\n");
	for(int i=0;i<n;i++) printf("%g %g %g\n",t[i],y[i],e[i]);
	printf("\n\n");

	struct exp_dat params;
	params.n=n;	
	params.t=t;	
	params.y=y;	
	params.e=e;

	const gsl_multimin_fminimizer_type* T = gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer* S = gsl_multimin_fminimizer_alloc(T,dim);

	gsl_multimin_function F;
	F.f = rad_func;
	F.n=dim;
	F.params=(void*)&params;

	//start parametres A=2, Time=2, B=2
	gsl_vector *start = gsl_vector_alloc(dim), *STEP =gsl_vector_alloc(dim);
	gsl_vector_set_all(start,2);
	gsl_vector_set_all(STEP, 2.0);

	gsl_multimin_fminimizer_set(S, &F, start, STEP);

	int flag; int iteration=0; double size;
	do
	{
		gsl_multimin_fminimizer_iterate(S);
		size = gsl_multimin_fminimizer_size(S);
		flag = gsl_multimin_test_size(size, 1e-12);
		iteration++;
	}while(flag==GSL_CONTINUE && iteration <100);

	double A=gsl_vector_get(S->x,0), Time=gsl_vector_get(S->x,1), B=gsl_vector_get(S->x,2);
	printf("# t, A*exp(-t/T)+B\n");
	double dt=(t[n-1]-t[0])/50;
	for(double ti=t[0]; ti<t[n-1]+dt; ti+=dt) printf("%g %g\n",ti,A*exp(-ti/ Time)+B);

	gsl_vector_free(start);
	gsl_vector_free(STEP);
	gsl_multimin_fminimizer_free(S);
	return 0;
}

