#include"gsl/gsl_multimin.h"

double ros_func(const gsl_vector *r, void *params)
{
	double x=gsl_vector_get(r,0), y=gsl_vector_get(r,1);
	return pow(1-x,2) + 100*pow(y-x*x,2);
}

int ros()
{
	int dim =2;
	const gsl_multimin_fminimizer_type* T = gsl_multimin_fminimizer_nmsimplex2;
	gsl_multimin_fminimizer* S = gsl_multimin_fminimizer_alloc(T,dim);
	
	gsl_multimin_function F;
	F.f = ros_func;
	F.n=dim;
	F.params=NULL;

	//start point x=(3,5)
	gsl_vector *start = gsl_vector_alloc(dim), *STEP =gsl_vector_alloc(dim);
	gsl_vector_set(start,0,3); gsl_vector_set(start,1,5);
	gsl_vector_set_all(STEP, 1.0);

	gsl_multimin_fminimizer_set(S, &F, start, STEP);

	double x=gsl_vector_get(S->x,0), y=gsl_vector_get(S->x,1);

	int flag; int iteration=0; double size;
	do
	{
		gsl_multimin_fminimizer_iterate(S);
		size = gsl_multimin_fminimizer_size(S);
		flag = gsl_multimin_test_size(size, 1e-12);
		iteration++;
		x=gsl_vector_get(S->x,0); y=gsl_vector_get(S->x,1);
		printf("%g %g %g\n", x, y, pow(1-x,2) + 100*pow(y-x*x,2) );

	}while(flag==GSL_CONTINUE);

	gsl_vector_free(STEP);
	gsl_vector_free(start);
	gsl_multimin_fminimizer_free(S);
		printf("\n\n");
	printf("Extremum\nx=%g y=%g\nNumber of iterations\n%i\n",x,y,iteration);
	return 0;

}
