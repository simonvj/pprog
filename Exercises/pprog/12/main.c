#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>
#include<stdio.h>
#include<stdlib.h>

double err_integrand(double x, void* params)
{
	return (2.0/sqrt(M_PI)) * exp(-x*x);
}

double errfunc(double x)
{
	gsl_function f;
	f.function = err_integrand;
	f.params = NULL;

	size_t limit = 100, key=5;
	double a=0, acc=1e-6, eps=1e-6, result, err;
	gsl_integration_workspace * workspace =	gsl_integration_workspace_alloc(limit);
	int status = gsl_integration_qag(&f,a,x,acc,eps,limit,key,workspace,&result,&err);
	
	gsl_integration_workspace_free(workspace);
	if(status!=GSL_SUCCESS) return NAN;
	else return result;
}

int main(int argc, char **argv)
{
	double a,b,dx;
	a=atof(argv[1]); b=atof(argv[2]); dx=atof(argv[3]);
	printf("# Exercise 1 \n");

	for (a = a; a < b; a += dx){printf("%g %g\n",a, errfunc(a));}

	return 0;
}
