#include <stdio.h>
#include <tgmath.h>

int main()
{
	printf("Part 1 of exercise math\n");
	double a=tgamma(5); double b=j1(0.5);
	printf("Gamma(5)=%g and e=%g\n Now J_1(0.5)=%g\n", a, M_E, b);
	double complex c=csqrt(-2.0); double d=sqrt(2);
	printf("csqrt(-2)=%.1f+%.1fi and the regular gives sqrt(2)=%g\n", creal(c), cimag(c), d);
	complex double e=cexp(I); double complex f=cexp(I*M_PI); double complex g=cpow(I,M_E);
	printf("Now Euler said: exp(i)=%.2f%+.2fi , exp(i*pi)=%.2f%+.2fi and i^e=%.2f%+.2f\n",creal(e), cimag(e), creal(f), cimag(f), creal(g), cimag(g)); 
	printf("Part 2 of exercise math\n");
	float h=0.11111111111111111111111111111111111; double i=0.1111111111111111111111111111111111; long double j=0.1111111111111111111111111111111111111111111111111111L;
	printf("%.25g.. Seems float can't story many decimals. Now double and long double.\n",h);
	printf("%.25lg and\n%.25Lg\n",i,j);	
	return 0;
}
