#include"limits.h"
#include"float.h"
#include"stdio.h"

int Part1(){
	printf("i)\n");

	int i=1;
	while(i+1>i){i++;}
	printf("My max int = %i\n", i);
	printf("Real max int = %i\n", INT_MAX);

	printf("ii)\n");

	i=1;
	while(i-1<i){i--;}
	printf("My min int = %i\n", i);
	printf("Real min  int = %i\n", i);

	printf("iii)\n");

	double x=1;
	while(1+x!=1){x/=2;} x*=2;
	printf("Epsilon double = %g compared to values from float.h %g\n", x, DBL_EPSILON);

	float a=1;
	while(1+a!=1){a/=2;} a*=2;
	printf("Epsilon float = %.8f compared to values from float.h %.8f\n", a, FLT_EPSILON);

	long double b=1;
	while(1+b!=1){b/=2;} b*=2;
	printf("Epsilon long double = %Lg compared to values from float.h %Lg\n", b, LDBL_EPSILON);

	return 0;
}