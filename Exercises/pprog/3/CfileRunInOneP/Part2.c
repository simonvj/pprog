#include"stdio.h"
#include"float.h"
#include"limits.h"

int Part2(){

	int max = INT_MAX/2; 
	float sum_up_float=0;
	float sum_down_float=0;

	printf("i)\n");

	for(int i=1;i<(max+1);i++){sum_up_float = sum_up_float+1.0f/i;}
	printf("float sum up = %.25f\n", sum_up_float);

	for(int i=0;i<max;i++){sum_down_float=sum_down_float+1.0f/(max-i);}
	printf("float sum down = %.25f\n", sum_down_float);
	
	printf("difference = %.25f\n", sum_up_float-sum_down_float);
	

	printf("ii)\n");
	printf("The differnce is, that when we start out at 1.0 we neglect the many last terms that are much smaller than the machine epsilon for float. When we start small we don't loose this in the same way.\n");
	printf("1/max=%.25f so is indeed very small.\n", 1.0f/max);

	printf("iii)\n");
	printf("This is the harmonic sum and does not converge if max goes to inf. However, when the number added is smaller than the machine epsilon, then the sum won't change and will indeed converge on the pc.\n");

	printf("iv)\n");

	double sum_up_double=0;
	double sum_down_double=0;

	for(int i=1;i<(max+1);i++){sum_up_double = sum_up_double+1.0/i;}
	printf("float sum up = %.25g\n", sum_up_double);

	for(int i=0;i<max;i++){sum_down_double=sum_down_double+1.0/(max-i);}
	printf("float sum down = %.25g\n", sum_down_double);
	
	printf("difference = %.25g\n", sum_up_double-sum_down_double);
	printf("The results are different because the machine epsilon is very different for double and float!\n");

	return 0;
}