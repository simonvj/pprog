#include"stdio.h"

int Part1();
int Part2();
int equal();
void name_digit();

int main(){
	printf("Part 1\n");
	Part1();
	printf("Part 2\n");
	Part2();
	printf("Part 3\n");
	equal(4.0,3.55,0.5,6e-6);
	printf("Part 4\n");
	name_digit(3);
	printf("It works\n");
	return 0;
}