#include"limits.h"
#include"float.h"
#include"stdio.h"

int main(){
	int i=1;
	while(i+1>i){i++;}
	printf("My max int = %i\n", i);
	printf("Real max int = %i\n", INT_MAX);
	i=1;
	while(i-1<i){i--;}
	printf("My min int = %i\n", i);
	printf("Real min  int = %i\n", i);
	return 0;
}