#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if(double_equal(vi, value)) printf("test passed\n");
	else printf("test failed\n");

	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	double d = 0.0;
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
		d = d + x*y;
	}

	printf("\nmain: testing nvector_dot_product ...\n");
	double u = 0;
	u = nvector_dot_product(a, b);
	printf("a*b should   =  %g\n", d);
	printf("a*b actually = %g\n", u);

	if (double_equal(u, d)) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_add ...\n");
	nvector_add(a, b);
	nvector_print("a+b should   = ", c);
	nvector_print("a+b actually = ", a);

	if (nvector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_set_zero ...\n");
	nvector_set_zero(a);
	nvector *e = nvector_alloc(n);
	for (int i = 0; i < n; ++i)
	{
		(*e).data[i]=0;
	}
	nvector_print("a should   = ", e);
	nvector_print("a actually = ", a);

	if (nvector_equal(e, a)) printf("test passed\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_scale ...\n");
	for (int i = 0; i < n; ++i)
	{
		(*e).data[i]= (*b).data[i]*u;
	}
	nvector_scale(b, u);
	nvector_print("a should   = ", e);
	nvector_print("a actually = ", b);

	if (nvector_equal(b, e)) printf("test passed\n");
	else printf("test failed\n");

	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);
	nvector_free(e);

	return 0;
}