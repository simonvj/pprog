#include"stdio.h"
#include"stdlib.h"
#include"assert.h"
#include"nvector.h"
#include"math.h"

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free(v->data); free(v);}

void nvector_set(nvector* v, int i, double value){ (*v).data[i]=value; }

double nvector_get(nvector* v, int i){return (*v).data[i]; }

void nvector_print(char *s, nvector* v)
{
	printf("%s", s);
	for (int i = 0; i < v->size; i++)
		printf("%9.3g ", v->data[i]);
	printf("\n");
}

int double_equal(double a, double b)
{
	double TAU = 1e-6, EPS = 1e-6;
	if (fabs(a - b) < TAU)
		return 1;
	if (fabs(a - b) / (fabs(a) + fabs(b)) < EPS / 2)
		return 1;
	return 0;
}

int nvector_equal(nvector* a, nvector* b)
{
	if (a->size != (*b).size) return 0;
	for(int i=0; i<(*a).size; i++)
		if(!double_equal((*a).data[i], (*b).data[i]))
			return 0;
	return 1;
}

double nvector_dot_product(nvector* u, nvector* v)
{
	assert(u->size == v->size);
	double c = 0.0;
	for(int i = 0; i < (*v).size; i++)
		c = c + u->data[i]*v->data[i];
	return c;
}

void nvector_add(nvector* v, nvector* u)
{
	assert((*v).size == (*u).size);
	for(int i=0; i<(*v).size; i++)
	{
		nvector_set(v, i, v->data[i] + u->data[i]);
	}
}

void nvector_set_zero(nvector* a)
{
	for(int i = 0; i<(*a).size; i++)
		nvector_set(a, i, 0.0);
}


void nvector_scale(nvector* a, double x)
{
	for (int i = 0; i < a->size; ++i)
	{
		a->data[i] = x*a->data[i];
	}
}