
main: testing nvector_alloc ...
test passed

main: testing nvector_set and nvector_get ...
test passed

main: testing nvector_dot_product ...
a*b should   =  1.58082
a*b actually = 1.58082
test passed

main: testing nvector_add ...
a+b should   =      1.18      1.71     0.533      1.05      1.03 
a+b actually =      1.18      1.71     0.533      1.05      1.03 
test passed

main: testing nvector_set_zero ...
a should   =         0         0         0         0         0 
a actually =         0         0         0         0         0 
test passed

main: testing nvector_scale ...
a should   =      1.24      1.44      0.53     0.439     0.755 
a actually =      1.24      1.44      0.53     0.439     0.755 
test passed
