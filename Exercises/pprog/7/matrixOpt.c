#include"gsl/gsl_matrix.h"
#include"gsl/gsl_vector.h"
#include"gsl/gsl_linalg.h"
#include"stdio.h"

int main()
{
	printf("We have Mx=b\n\n");

	int n = 3;
	gsl_matrix* M = gsl_matrix_calloc(n,n);
	gsl_matrix_set(M, 0,0,6.13);
	gsl_matrix_set(M, 1,0,-2.90);
	gsl_matrix_set(M, 2,0,5.86);
	gsl_matrix_set(M, 0,1,8.08);
	gsl_matrix_set(M, 1,1,-6.31);
	gsl_matrix_set(M, 2,1,-3.89);
	gsl_matrix_set(M, 0,2,-4.36);
	gsl_matrix_set(M, 1,2,1.00);	
	gsl_matrix_set(M, 2,2,0.19);

	printf("M=\n");

	for(int j=0;j<M->size2;j++){
		for(int i=0;i<M->size1;i++)
			printf("%8.3g ",gsl_matrix_get(M,i,j));
		printf("\n");
		}
	
	gsl_vector* b = gsl_vector_calloc(n);
	gsl_vector_set(b, 0, 6.23);
	gsl_vector_set(b, 1, 5.37);
	gsl_vector_set(b, 2, 2.29);

	printf("b=\n");

	for (int i = 0; i < b->size; i++)
	{
		printf("%8.3g\n", gsl_vector_get(b,i));
	}
	printf("\n");

	gsl_vector* x = gsl_vector_calloc(n);

	gsl_matrix* A = gsl_matrix_calloc(n,n);
	gsl_matrix_memcpy(A,M);

	gsl_linalg_HH_solve(A, b, x);

	printf("The solution is then \n\nx=\n");
	for (int i = 0; i < n; i++)
	{
		printf("%8.3g\n", gsl_vector_get(x,i));
	}

	printf("\nChecking this solution is correct..");

	gsl_vector* v = gsl_vector_calloc(n);

	printf("Now we multiply the matrix with to solution to see if it matches b..\n\nv=Mx=\n");

	double temp = 0.0;

	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			temp += gsl_matrix_get(M,i,j)*gsl_vector_get(x,j);
		}
		gsl_vector_set(v,i,temp);
		temp=0.0;
		printf("%8.3g\n", gsl_vector_get(v,i));
	}



	gsl_matrix_free(M);
	gsl_vector_free(b);
	gsl_vector_free(x);
	gsl_vector_free(v);
}