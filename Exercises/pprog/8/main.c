#include"stdio.h"
#include"math.h"
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int almen_ode(double x, const double y[], double dydx[], void* params)
{
	dydx[0]=y[0]*(1.0-y[0]);
	return GSL_SUCCESS;
}

double LogisFun(double x)
{
	return 1/(1+exp(-x));
}

double solve_almen_ode(double x)
{
	gsl_odeiv2_system sys;
	sys.function = almen_ode;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	double hstart = copysign(0.1, x);
	double acc = 1e-6;
	double eps = 1e-6;
	gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45, hstart, acc, eps);

	double t = 0.0;
	double y[1]={0.5};
	gsl_odeiv2_driver_apply(driver, &t, x, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main()
{
	for(double x = 0.0; x < 3.0; x += 0.1)
	{
		printf("%g %g\n", x, solve_almen_ode(x));
	}
	printf("\n\n");
	for(double i = 0.0; i < 3.0; i += 0.01)
	{
		printf("%g %g\n", i, LogisFun(i) );
	}
	return 0;

}