#include"stdio.h"
#include"math.h"
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int odefun(double x, const double y[], double dy2dx2[], void* params)
{
	double epsilon = *(double *) params;
	dy2dx2[0] = y[1];
	dy2dx2[1]=-y[0] + 1.0 + epsilon*y[0]*y[0];
	return GSL_SUCCESS;
}

double solve_planet_motion(double x, double epsilon, double u0, double u0prime)
{
	gsl_odeiv2_system sys;
	sys.function = odefun;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void *) &epsilon;

	double hstart = copysign(0.1, x);
	double acc = 1e-6;
	double eps = 1e-6;
	gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45, hstart, acc, eps);

	double t = 0.0;
	double y[2]={u0, u0prime};
	gsl_odeiv2_driver_apply(driver, &t, x, y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main()
{
	for(double x = 0.0; x < 20*M_PI; x += 0.1)
	{
		printf("%g %g %g %g\n",x, solve_planet_motion(x,0,1.0, 0.0) , solve_planet_motion(x,0,1.0, -0.5),solve_planet_motion(x,0.01,1.0, -0.5) );
	}
	return 0;
}