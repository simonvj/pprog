#include"stdio.h"
#include"math.h"
#include"gsl/gsl_integration.h"

double log_integrand(double x, void* params)
{
	return log(x)/sqrt(x);
}

double integ(double a, double b)
{
	gsl_function f;
	f.function = &log_integrand;
	f.params = NULL;;

	size_t limit = 87;
	double acc = 1e-6, eps = 1e-6, result, err;
	gsl_integration_workspace * workspace =	gsl_integration_workspace_alloc(limit);
	gsl_integration_qags(&f, a, b, acc, eps, limit, workspace, &result, &err); 

	gsl_integration_workspace_free(workspace);

	return result;
}