#include"stdio.h"
#include"math.h"
#include"stdlib.h"

double integ(double, double);
double quantumenergy(double);

int main(){
	printf("Exercise 1 \n\n");

	double a=0,b=1;
	printf("Int = %g \n",integ(a,b));


	printf("\nExercise 2 \n\n");
	
	a = 1.0;
	printf("E_min = %g\n", quantumenergy(a) );


	return 0;
}
