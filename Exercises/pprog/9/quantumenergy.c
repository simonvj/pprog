#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double hamilton_integrand(double x, void* params){
	double a = *(double*)params;
	return (-pow(a*x,2)/2.0 + a/2.0 + pow(x,2)/2.0)*exp(-a*pow(x,2));
}

double norm_integrand(double x, void* params){
	double a = *(double*)params;
	return exp(-a*pow(x,2));
}

double quantumenergy(double a){
	gsl_function f1;
	f1.function = norm_integrand;
	f1.params = (void*)&a;

	gsl_function f2;
	f2.function = hamilton_integrand;
	f2.params = (void*)&a;

	int limit = 100;
	double b=0, acc=1e-6, eps=1e-6, result1, result2, err;
	gsl_integration_workspace * workspace =	gsl_integration_workspace_alloc(limit);
	int status = gsl_integration_qagiu(&f1,b,acc,eps,limit,workspace,&result1,&err);

	status = gsl_integration_qagiu(&f2, b, acc, eps, limit, workspace, &result2, &err );
	
	gsl_integration_workspace_free(workspace);
	if(status!=GSL_SUCCESS) return NAN;
	else return result2/result1;
}