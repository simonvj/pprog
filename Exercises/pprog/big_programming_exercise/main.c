#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_eigen.h>
#include<stdio.h>


void eigenval(double x)
{

	int dim=2;
	gsl_matrix* H = gsl_matrix_calloc(dim,dim);
			gsl_matrix_set(H,0,0,1.0);
			gsl_matrix_set(H,0,1,x);
			gsl_matrix_set(H,1,0,x);
			gsl_matrix_set(H,1,1,-1.0);


	gsl_vector* sol = gsl_vector_alloc(dim);
	gsl_eigen_symm_workspace * w=gsl_eigen_symm_alloc(dim);
	gsl_eigen_symm(H,sol,w);


	printf("%g %g %g %g %g\n",
		x,
		gsl_vector_get(sol,0),
		gsl_vector_get(sol,1),
		sqrt(pow(x,2)+1.0),
		-sqrt(pow(x,2)+1.0));


	gsl_eigen_symm_free(w);
	gsl_matrix_free(H);
	gsl_vector_free(sol);
}



int main(void) 
{
	double xmin=-5.0, xmax=5.0, dx=0.1;
	printf("#x\tgsl1\tgsl2\tanalytical1\tanalytical2\n");
	for(double x = xmin; x < xmax + dx;x += dx){
		eigenval(x);
	}

	return 0;
}