
typedef struct {int n, double *x, *y, *b, *c} qspline;
qspline* qspline_alloc(int n, double *x, *y);
double qspline_eval(qspline* S,double z);
void qspline_free(qspline* S);
