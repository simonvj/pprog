#include"math.h"
#include"stdio.h"

double root(double);

int main()
{
	for(double x = 0; x < 17; x += 0.2)
		printf("%g %g\n", x, root(x));
	printf("\n\n");
	for(double x = 0; x < 17; x += 0.5)
		printf("%g %g\n", x, sqrt(x));
}