#include"gsl/gsl_multiroots.h"
#include"gsl/gsl_vector.h"
#include"assert.h"

int root_equation(const gsl_vector* v, void* params, gsl_vector* f)
{
	double z = *(double*)params;
	double x = gsl_vector_get(v,0);
	gsl_vector_set(f, 0, x*x-z);
	return GSL_SUCCESS;
}

double root(double z)
{
	assert(z>=0);
	if(z>4)return 2*root(z/4);

	int dim = 1;
	const gsl_multiroot_fsolver_type* T = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver* S = gsl_multiroot_fsolver_alloc(T,dim);

	gsl_multiroot_function F;
	F.f=root_equation;
	F.n=dim;
	F.params=(void*)&z;

	gsl_vector* start = gsl_vector_alloc(dim);
	gsl_vector_set(start, 0, 1.0);
	gsl_multiroot_fsolver_set(S, &F, start);

	int flag;
	do
	{
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f, 1e-12);
	}
	while(flag==GSL_CONTINUE);

	double result = gsl_vector_get(S->x,0);

	gsl_vector_free(start);
	gsl_multiroot_fsolver_free(S);
	
	return result;
}