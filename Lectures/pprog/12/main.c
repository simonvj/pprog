#include"stdio.h"
#include"stdlib.h"

int main(int argc, char** argv)
{
	if(argc<2)
	{
		fprintf(stderr, "usage: %s number_of_points < file_with_points\n", argv[0]);
		return 1;
	}
	int n=atoi(argv[1]);
	double e[n],s[n],d[n];
	fprintf(stderr, "n=%i\n", n);
	for(int i=0; i<n;i++)
		scanf("%lg %lg %lg", &(e[i]), &(s[i]), d+i);

	for(int i=0; i<n;i++)
		printf("%g %g %g\n", e[i], s[i], d[i]);
	return 0;
}