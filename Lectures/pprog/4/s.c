#include"stdio.h"
#include"komplex.h"
struct person {int age; char* name;};

struct point
{
double x,y;	
};

void bar(struct point * p){
printf("p.x=%g\n", (*p).x );
printf("p.y=%g\n", p->y );

}

int main(){
	char name[] = "john";
	struct person john;
	john.name=name;
	john.age=20;
	printf("age=%i\n", john.age);
	printf("name=%s\n", john.name);
	struct point p={1,2};
	printf("%g %g\n",p.x,p.y );
	bar(&p);
}