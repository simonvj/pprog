#include"stdio.h"
#include"math.h"

struct F {double (*f1)(double); double (*f2)(double);};

double RieSum(double (*f)(double), double a, double b, int n){
	if(n<3)n=3;
	double dx=(b-a)/(n-1), s=(f(a)+f(b))/2;
	for(int i=1; i<n; i++)s+=f(a+i*dx);
		return s*dx;
}

int main(int argc, char** argv){
		int n=3;
		if(argc>1)n=atoi(argv[1]);
		double x=1;
		double (*f[3])(double)={sin,cos,tan};
		f[0]=&exp; /* can say f=sin; f(x) as well!*/
		for(int i=0; i<3; i++)
		printf("f(1)=%g\n", f[i](x));

		struct F bar = {sin, cos};
		printf("bar.f1=%g bar.f2=%g\n", bar.f1(x),bar.f2(x) );

		printf("n=%i\n",n );
		double s = RieSum(sin,0,M_PI,n);
		printf("Riesum = %g\n", s);

		return 0;
}