#include"stdio.h"
#define EPS 1e-6
#if !defined NDEBUG
#define dtrace(x) printf("%s = %lg\n", #x, x );
#else
#define dtrace(...)
#endif
int main(){
	double x=1.23;
	dtrace(x);
	dtrace(EPS);
	dtrace(1e-6);
}