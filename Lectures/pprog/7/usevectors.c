#include"gsl/gsl_vector.h"
#include"gsl/gsl_matrix.h"
#include"gsl/gsl_blas.h"
#define RAND (double)rand()/RAND_MAX
int main()
{
	int n = 3;
	gsl_vector* v = gsl_vector_calloc(n);
	gsl_vector_set(v,0,RAND);
	gsl_vector_set(v,1,RAND);
	gsl_vector_set(v,2,RAND);
	gsl_vector_fprintf(stdout,v,"%g");
	for(int i=0; i<v->size;i++)
		printf("v[%i]=%g\n", i,gsl_vector_get(v,i));
	double result;
	int flag = gsl_blas_ddot(v,v,&result);
	printf("flag = %i, result of dot product = %g\n", flag, result);

	gsl_vector_free(v);
	return 0;
}	