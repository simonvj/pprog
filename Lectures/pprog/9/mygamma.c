#include"stdio.h"
#include"math.h"
#include"gsl/gsl_integration.h"

double gamma_integrand(double x, void* params)
{
	double z=*(double*)params;
	return pow(x,z-1)*exp(-x);
}

double mygamma(double z)
{
	gsl_function f;
	f.function = &gamma_integrand;
	f.params = (void*)&z;

	size_t limit = 100;
	double acc = 1e-6, eps = 1e-6, result, err;
	gsl_integration_workspace* workspace = gsl_integration_workspace_alloc(limit);
	gsl_integration_qagiu(&f, 0, acc, eps, limit, workspace, &result, &err); 

	gsl_integration_workspace_free(workspace);
	return result;
}