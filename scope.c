#include"stdio.h"

int N=1000;
/* This N is in file-scope, it is visible to all functions inside
this file (unless shadowed). */

void hello(void) /* also file scope */
{
	char* greeting="hello";
/* Variable 'greeting' is in function-scope, it is visible only inside
function hello; it gets deleted when hello exits. */

	printf("%s",greeting);
	printf(" N=%i\n",N);     /* Prints N from the file scope. */
}

int main(void)
{
	int N=10;
/* This N is in function-scope and shadows the file-scope N. This N
replaces the file-scope N inside main. */

	printf("N=%i\n",N);  /* Prints N from the function-scope. */

	hello(); /* hello will print N from the file-scope. */

	for(int N=1;N<10;N++)
/* This N is in block-scope, it is only visible inside the block. */

	{

		printf("%i\n",N); /* The block-scope N shadows the function-scope N. */

	}
return 0;
}

